// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
import express from 'express';
import cors from 'cors';
import initializeExpressMiddleware from './loaders/expressLoader';
import connectToTheDatabase from './loaders/mongooseLoader';

// Creating function because we cannot use await directly in the top level code.
// Refer https://stackoverflow.com/questions/46515764/how-can-i-use-async-await-at-the-top-level/46515787#46515787
async function startServer() {
    const app: express.Application = express();

    app.use(cors())
    // Connect to the Database
    await connectToTheDatabase();

    // Initialize express middlewares
    initializeExpressMiddleware(app);

    // Start Server at Port
    const port: number = parseInt(process.env.PORT) || 8080;
    app.listen(port, () => {
        console.log(`Server started at Port: ${port}`);
    });
}

startServer().catch((e) => {
    console.error(e);
});
