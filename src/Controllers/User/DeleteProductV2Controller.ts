import {  Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { ProductNotFoundException } from './UserException';
import { productsModel } from '../../Models/Products/ProductsModel';

export default async function DeleteProductV2Controller(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.pid;

    try {
        const product = await productsModel.findById(id);
        if (!product) throw new ProductNotFoundException();
        await product.deleteOne();
        response.send(product);
    } catch (error) {
        next(error);
    }
}

