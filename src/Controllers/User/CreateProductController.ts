import { Request, Response, NextFunction } from 'express';
import { userModel } from '../../Models/User/UserModel';
import { plainToClass } from 'class-transformer';
import { PasswordService } from '../../Utils/PasswordService';
import { UserWithThatEmailAlreadyExistsException } from '../Admin/AdminException';
import GenerateAccessAndRefreshToken, { IAccessAndRefreshToken } from '../Shared/GenerateAccessAndRefreshToken';
import RegisterUserProps from './props/RegisterUserProps';
import { productModel } from '../../Models/Product/ProductModel';
import CreateProductProps from './props/CreateProductProps';

export default async function CraeteProductController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(CreateProductProps, request.body);

    try {
        const user = await productModel.create({
            ...props
        });

        response.send(user);
    } catch (error) {
        next(error);
    }
}
