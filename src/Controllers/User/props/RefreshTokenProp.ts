import { IsDefined, IsJWT } from 'class-validator';

export class RefreshTokenProp {
    @IsDefined()
    @IsJWT()
    refreshToken: string;
}
