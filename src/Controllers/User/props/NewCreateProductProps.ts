// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsDefined,
    IsBoolean,
    IsOptional
} from 'class-validator';


class CreateProductPropsv2 {
    @IsDefined()
    @IsString()
    userId: string;

    @IsDefined()
    @IsString()
    productName: string;
    
     
    @IsDefined()
    @IsString()
    @IsOptional()
    productDescription?: string;
    
    @IsDefined()
    @IsString()
    productCategory: string;
    
    @IsDefined()
    @IsString()
    productPrice: string;
    
    @IsDefined()
    @IsString()
    productImg: string;
    
    @IsDefined()
    @IsString()
    @IsOptional()
    productBackImg?: string;

    @IsDefined()
    @IsString()
    @IsOptional()
    productProps?: Map<string,string>;
    
    @IsDefined()
    @IsBoolean()
    @IsOptional()
    isActive?: boolean;
        
    @IsDefined()
    @IsBoolean()
    isWriter: boolean;
    
}


export default CreateProductPropsv2;
