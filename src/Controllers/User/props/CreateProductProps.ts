// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsDefined,
} from 'class-validator';


class CreateProductProps {
    @IsDefined()
    @IsString()
    userId: string;
    

    @IsDefined()
    @IsString()
    productImg: string;
    
    @IsDefined()
    @IsString()
    productBackImg: string;

    @IsDefined()
    @IsString()
    productCaption: string;

    @IsDefined()
    @IsString()
    productPrice: string;
    
    @IsDefined()
    @IsString()
    productProps: Map<string,string>;
}


export default CreateProductProps;
