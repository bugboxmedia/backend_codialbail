// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import { Type } from 'class-transformer';
import {
    IsString,
    Matches,
    IsDefined,
    IsPhoneNumber,
    IsNotEmptyObject,
    ValidateNested,
    IsEnum,
    MinLength,
    IsISO8601
} from 'class-validator';
import { validEmailRegex } from '../../../Utils/helper';
import { userModelConditions } from '../../../Models/User/UserModelConditions';

class RegisterUserProps {
    @IsDefined()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => NameProps)
    name: NameProps;

    @IsDefined()
    @Matches(validEmailRegex)
    email: string;

    @IsDefined()
    @IsPhoneNumber('ZZZ')
    phoneNumber: string;

    @IsDefined()
    @IsString()
    @MinLength(userModelConditions.password.minLength)
    password: string;

    @IsDefined()
    @IsString()
    @IsEnum(userModelConditions.gender.permittedStrings, {
        message: '$property should be one of [$constraint1]'
    })
    gender: 'male' | 'female' | 'others';

}

class NameProps {
    @IsDefined()
    @IsString()
    firstName: string;

    @IsDefined()
    @IsString()
    lastName: string;
}

export default RegisterUserProps;
