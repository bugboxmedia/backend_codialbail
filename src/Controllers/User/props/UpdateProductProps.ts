// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsOptional,
    IsNotEmptyObject,    
} from 'class-validator';

class UpdateProductProps {
    @IsOptional()
    userId: string;

    @IsOptional()
    @IsNotEmptyObject()
    productProps: Map<string, string>;

    @IsOptional()
    @IsString()
    productImg: string;

    @IsOptional()
    @IsString()
    productCaption: string;

    @IsOptional()
    @IsString()
    productPrice: string;

}


export default UpdateProductProps;
