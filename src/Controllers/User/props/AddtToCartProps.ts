// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsDefined,
} from 'class-validator';


class AddToCartProps {
    @IsDefined()
    @IsString()
    productId: string;
    

    @IsDefined()
    quantity: number;
  
}


export default AddToCartProps;
