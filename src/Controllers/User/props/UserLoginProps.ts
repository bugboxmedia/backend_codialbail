import { IsString, Matches, IsOptional, IsDefined } from 'class-validator';
import { validEmailRegex } from '../../../Utils/helper';

class UserLoginProps {
    @IsDefined()
    @Matches(validEmailRegex)
    email: string;

    @IsDefined()
    @IsString()
    password: string;
}

export default UserLoginProps;
