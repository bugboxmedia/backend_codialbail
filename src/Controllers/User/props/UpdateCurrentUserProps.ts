// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import { Type } from 'class-transformer';
import {
    IsString,
    IsOptional,
    IsNotEmptyObject,
    ValidateNested,
    IsPhoneNumber,
} from 'class-validator';
import { userModelConditions } from '../../../Models/User/UserModelConditions';
import { validEmailRegex } from '../../../Utils/helper';

class UpdateUserProps {
    @IsOptional()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => NameProps)
    name?: NameProps;

    // @IsOptional()
    // @Matches(validEmailRegex)
    // email?: string;

    @IsOptional()
    @IsPhoneNumber('ZZZ')
    phoneNumber?: string;

    // @IsOptional()
    // @IsString()
    // @MinLength(userModelConditions.password.minLength)
    // oldPassword?: string;

    // @IsOptional()
    // @IsString()
    // @MinLength(userModelConditions.password.minLength)
    // newPassword?: string;

    // @IsOptional()
    // @IsString()
    // @IsEnum(userModelConditions.gender.permittedStrings, {
    //     message: '$property should be one of [$constraint1]'
    // })
    // gender?: 'male' | 'female' | 'others';


    // @IsOptional()
    // @IsISO8601({ strict: true })
    // dateOfBirth?: Date;

    // @IsOptional()
    // @IsBoolean()
    // isActive?: boolean;

    // this field is not required to be present in the request. This is here
    // only for typescript intellisense and to avoid error.
    // password?: string;

}

class NameProps {
    @IsOptional()
    @IsString()
    firstName: string;

    @IsOptional()
    @IsString()
    lastName: string;
}

export default UpdateUserProps;
