import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import UpdateUserUseCase from '../Shared/UpdateUserUseCase';
import { UserNotFoundException } from '../User/UserException';
import UpdateOrderUseCase from './UpdateOrderUseCase';
import UpdateOrderProps from '../Shared/SharedProps/UpdateOrderProps';

export default async function UpdateUserController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.oid;
    const props = plainToClass(UpdateOrderProps, request.body);

    try {
        const user = await UpdateOrderUseCase(id, props);

        if (!user) throw new UserNotFoundException();

        response.send(user);
    } catch (error) {
        next(error);
    }
}
