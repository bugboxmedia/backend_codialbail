import {  Response, NextFunction } from 'express';
import { productModel } from '../../Models/Product/ProductModel';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { ProductNotFoundException } from './UserException';

export default async function DeleteProductController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.pid;

    try {
        const product = await productModel.findById(id);
        if (!product) throw new ProductNotFoundException();
        await product.deleteOne();
        response.send(product);
    } catch (error) {
        next(error);
    }
}

