// import { Request, Response, NextFunction } from 'express';
// import { plainToClass } from 'class-transformer';
// import { userModel } from '../../Models/User/UserModel';
// import { UserWithPhoneNumberNotFoundException } from './UserException';
// import GenerateAccessAndRefreshToken, { IAccessAndRefreshToken } from '../Shared/GenerateAccessAndRefreshToken';
// import { IsDefined, Matches, IsNumber, Min, IsString, MinLength } from 'class-validator';
// import { validPhoneNumberRegex, validPhoneOtpRegex } from '../../Utils/helper';
// import { PhoneOtpModelConditions } from '../../Models/PhoneOtp/IPhoneOtpProps';
// import { PhoneOtpService } from '../../Utils/PhoneOtpService';
// import { userModelConditions } from '../../Models/User/UserModelConditions';
// import { PasswordService } from '../../Utils/PasswordService';

// export class ResetPasswordProps {
//     @IsDefined()
//     @Matches(validPhoneNumberRegex)
//     phoneNumber: string;

//     @IsDefined()
//     @Matches(validPhoneOtpRegex)
//     otp: string;

//     @IsDefined()
//     @IsString()
//     @MinLength(userModelConditions.password.minLength)
//     password: string;
// }

// export default async function ResetPasswordController(request: Request, response: Response, next: NextFunction) {
//     const props = plainToClass(ResetPasswordProps, request.body);

//     try {
//         // If any error the promise throws error
//         await PhoneOtpService.verifyOTP(props.phoneNumber, props.otp);

//         const hashedPassword = PasswordService.hash(props.password);

//         // If OTP is verified, we update the password of the user.
//         const user = await userModel.findOneAndUpdate(
//             { phoneNumber: props.phoneNumber },
//             { password: hashedPassword },
//             { new: true }
//         );

//         if (!user) throw new UserWithPhoneNumberNotFoundException(props.phoneNumber);

//         const tokens: IAccessAndRefreshToken = await GenerateAccessAndRefreshToken(user);

//         const data = {
//             user: user,
//             token: tokens
//         };

//         response.send(data);
//     } catch (error) {
//         next(error);
//     }
// }
