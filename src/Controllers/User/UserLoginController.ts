import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { userModel } from '../../Models/User/UserModel';
import { PasswordService } from '../../Utils/PasswordService';
import {
    UserWithEmailNotFoundException,
    InvalidPasswordException,
    UserIsDeactivatedException,
    EmailOrPhoneNumberRequiredException,
    UserWithPhoneNumberNotFoundException
} from './UserException';
import GenerateAccessAndRefreshToken, { IAccessAndRefreshToken } from '../Shared/GenerateAccessAndRefreshToken';
import UserLoginProps from './props/UserLoginProps';

export default async function UserLoginController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(UserLoginProps, request.body);

    // If both email and phoneNumber is not provided we throw error
    // if (!props.email && !props.phoneNumber) throw new EmailOrPhoneNumberRequiredException();

    try {

        const user = await userModel.findOne({ email: props.email }).select('+password');

        if (!user) throw new UserWithEmailNotFoundException(props.email);

        const hashedPassword = user.password;

        const isPasswordMatch = await PasswordService.verify(props.password, hashedPassword);

        if (!isPasswordMatch) throw new InvalidPasswordException();

        if (!user.isActive) throw new UserIsDeactivatedException();

        const tokens: IAccessAndRefreshToken = await GenerateAccessAndRefreshToken(user);

        const data = {
            data: user,
            token: tokens
        };

        response.send(data);
    } catch (error) {
        next(error);
    }
}
