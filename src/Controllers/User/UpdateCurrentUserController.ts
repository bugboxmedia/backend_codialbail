import { Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import UpdateCurrentUserProps from './props/UpdateCurrentUserProps';
import { OldPasswordIsIncorrectException, OldPasswordIsRequiredException, UserNotFoundException } from './UserException';
import { userModel } from '../../Models/User/UserModel';
import { UserWithThatEmailAlreadyExistsException } from '../Admin/AdminException';
import { PasswordService } from '../../Utils/PasswordService';

export default async function UpdateCurrentUserController(
    request: AuthRequest,
    response: Response,
    next: NextFunction
) {
    const id = request.userId;
    const props = plainToClass(UpdateCurrentUserProps, request.body);

    try {
        const user = await userModel.findById(id).select('+password');

    if (!user) throw new UserNotFoundException();

   

    // This is applicable only for retail users and not for dashboard staffs
    

    // if (props.newPassword) {
    //     if (!props.oldPassword) throw new OldPasswordIsRequiredException();

    //     // check if the old password matches with the password saved in the db.
    //     const isPasswordMatched = await PasswordService.compare(props.oldPassword, user.password);

    //     if (!isPasswordMatched) throw new OldPasswordIsIncorrectException();
    //     props.password = PasswordService.hash(props.newPassword);
    // }

    const updatedUser = await userModel.findByIdAndUpdate(id, props, { new: true });

    return updatedUser;
    } catch (error) {
        next(error);
    }
}
