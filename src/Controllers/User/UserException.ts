import HttpException from '../../Exceptions/HttpException';

export class UserWithEmailNotFoundException extends HttpException {
    constructor(email: string) {
        super(404, `User with email ${email} not found`);
    }
}

export class UserWithPhoneNumberNotFoundException extends HttpException {
    constructor(phoneNumber: string) {
        super(404, `User with phoneNumber ${phoneNumber} not found`);
    }
}

export class UserNotFoundException extends HttpException {
    constructor() {
        super(401, 'User Not Found');
    }
}

export class SaleItemNotFoundException extends HttpException {
    constructor() {
        super(401, 'Item Not Found');
    }
}
export class OrderNotFoundException extends HttpException {
    constructor() {
        super(400, 'Order Not Found');
    }
}
export class AdvNotFoundException extends HttpException {
    constructor() {
        super(400, 'Adv Not Found');
    }
}
export class ProductNotFoundException extends HttpException {
    constructor() {
        super(400, 'Product Not Found');
    }
}
export class AddressNotFoundException extends HttpException {
    constructor() {
        super(400, 'Address Not Found');
    }
}

export class InvalidPasswordException extends HttpException {
    constructor() {
        super(401, 'Password Invalid');
    }
}

export class UserIsDeactivatedException extends HttpException {
    constructor() {
        super(401, 'User Is Deactivated');
    }
}

export class EmailOrPhoneNumberRequiredException extends HttpException {
    constructor() {
        super(400, 'Either email or phoneNumber is required');
    }
}

export class OldPasswordIsIncorrectException extends HttpException {
    constructor() {
        super(400, 'Old Password is Incorrect');
    }
}

export class OldPasswordIsRequiredException extends HttpException {
    constructor() {
        super(400, 'oldPassword is required along with newPassword.');
    }
}

export class OtpIsRequiredWithPhoneException extends HttpException {
    constructor() {
        super(400, 'otp is required along with phoneNumber');
    }
}
