import { Request, Response, NextFunction } from 'express';
import { refreshTokenModel } from '../../Models/RefreshToken/RefreshTokenModel';
import { plainToClass } from 'class-transformer';
import { RefreshTokenProp } from './props/RefreshTokenProp';
import { JWTClaims } from '../../Utils/AuthService/IJwt';
import { AuthService } from '../../Utils/AuthService/AuthService';

export default async function UserLogoutController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(RefreshTokenProp, request.body);

    try {
        const userClaims: JWTClaims = await AuthService.decodeRefreshToken(props.refreshToken);

        await refreshTokenModel.findOneAndDelete({ userId: userClaims._id });

        response.sendStatus(204);
    } catch (error) {
        next(error);
    }
}
