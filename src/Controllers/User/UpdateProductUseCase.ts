
import {
    AddressNotFoundException, ProductNotFoundException,
} from './UserException';
import { productModel, productModelType } from '../../Models/Product/ProductModel';
import UpdateProductProps from './props/UpdateProductProps';

export default async function UpdateProductUseCase(id: string, props: UpdateProductProps): Promise<productModelType> {
    const Product = await productModel.findById(id);

    if (!Product) throw new ProductNotFoundException();

    const updatedProduct = await productModel.findByIdAndUpdate(id, props, { new: true });

    return updatedProduct;
}
