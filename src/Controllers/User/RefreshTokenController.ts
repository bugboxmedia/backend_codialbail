import { Request, Response, NextFunction } from 'express';
import { RefreshTokenProp } from './props/RefreshTokenProp';
import { plainToClass } from 'class-transformer';
import { AuthService } from '../../Utils/AuthService/AuthService';
import { JWTClaims } from '../../Utils/AuthService/IJwt';
import { refreshTokenModel } from '../../Models/RefreshToken/RefreshTokenModel';
import InvalidAuthTokenException from '../../Exceptions/InvalidAuthTokenException';
import GenerateAccessAndRefreshToken, { IAccessAndRefreshToken } from '../Shared/GenerateAccessAndRefreshToken';

export default async function RefreshTokenController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(RefreshTokenProp, request.body);

    try {
        const userClaims: JWTClaims = await AuthService.decodeRefreshToken(props.refreshToken);

        const refreshTokenExists = await refreshTokenModel.findOne({ userId: userClaims._id });

        if (!refreshTokenExists || !(props.refreshToken === refreshTokenExists.token))
            throw new InvalidAuthTokenException();

        const hasExpired = new Date(Date.now()) >= refreshTokenExists.expires;

        if (hasExpired) throw new InvalidAuthTokenException('Token Expired');

        const tokens: IAccessAndRefreshToken = await GenerateAccessAndRefreshToken(userClaims);

        response.send({ token: tokens });
    } catch (error) {
        next(error);
    }
}
