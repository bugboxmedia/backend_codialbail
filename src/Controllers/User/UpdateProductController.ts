import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { UserNotFoundException } from '../User/UserException';
import UpdateProductProps from './props/UpdateProductProps';
import UpdateProductUseCase from './UpdateProductUseCase';

export default async function UpdateProductController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.pid;
    const props = plainToClass(UpdateProductProps, request.body);

    try {
        const user = await UpdateProductUseCase(id, props);

        if (!user) throw new UserNotFoundException();

        response.send(user);
    } catch (error) {
        next(error);
    }
}
