// Create Admin Controller Buisness Logic

import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { userModel } from '../../Models/User/UserModel';
import { UserNotFoundException } from './UserException';

export default async function GetCurrentUserController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.userId;

    try {
        const user = await userModel.findById(id);

        if (!user) throw new UserNotFoundException();

        response.send(user);
    } catch (error) {
        next(error);
    }
}
