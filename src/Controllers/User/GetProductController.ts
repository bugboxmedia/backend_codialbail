import { Request, Response, NextFunction } from 'express';
import { userModel } from '../../Models/User/UserModel';
import { plainToClass } from 'class-transformer';
import { PasswordService } from '../../Utils/PasswordService';
import { UserWithThatEmailAlreadyExistsException } from '../Admin/AdminException';
import GenerateAccessAndRefreshToken, { IAccessAndRefreshToken } from '../Shared/GenerateAccessAndRefreshToken';
import RegisterUserProps from './props/RegisterUserProps';
import { productModel } from '../../Models/Product/ProductModel';
import CreateProductProps from './props/CreateProductProps';
import { ProductNotFoundException } from './UserException';

export default async function GetProductController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.pid;

    try {
        const product = await productModel.findById(id);

        if (!product) throw new ProductNotFoundException();

        response.send(product);
    } catch (error) {
        next(error);
    }
}
