import { Response, NextFunction } from 'express';
import { productsModel } from '../../Models/Products/ProductsModel';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { ProductNotFoundException, UserNotFoundException } from './UserException';
import { userModel } from '../../Models/User/UserModel';
import AddToCartProps from './props/AddtToCartProps';
import { plainToClass } from 'class-transformer';

export default async function AddToCartController(request: AuthRequest, response: Response, next: NextFunction) {
    const user_id = request.userId;
    const props = plainToClass(AddToCartProps, request.body);

    try {

        const product = await productsModel.findById(props.productId);
        if (!product) throw new ProductNotFoundException();

        var productPrice = product.productPrice
        const user = await userModel.findById(user_id).select('+password');

        if (!user) throw new UserNotFoundException();
        let existingCart:any = {
            "items" : [],
            "totalPrice": 0.0,
            "itemCount": 0
        }
        if(user['cart']){
            existingCart = user.cart
        }
        let updatedExistingCart = false
        existingCart['items'].forEach((item) => {
            if (item.id === props.productId) {
                item.quantity =props.quantity
                
                updatedExistingCart = true
            }
            
        })

        if (!updatedExistingCart) {
            existingCart['items'].push({
                "id": props.productId,
                "productName": product.productName,
                "productPrice": productPrice,
                "quantity": props.quantity
            })    
        }
        if (updatedExistingCart) {
            let count = 0;
            let tPrice = 0.0;
            existingCart['items'].forEach((item) =>{
                count += item['quantity']
                tPrice = tPrice + (item['productPrice'] * item['quantity'])
            })
            existingCart['itemCount'] = count;
            existingCart['totalPrice'] = tPrice;
        }
        else {
            // Quantity
            existingCart['itemCount'] = existingCart['itemCount'] + props.quantity;

            // Quantity * price
            existingCart['totalPrice'] += (parseInt(productPrice) * props.quantity)
        }

        // User update with cart object

        const updatedUser = await userModel.findByIdAndUpdate(user_id, {
            cart: existingCart
        }, { new: true });
        // await product.deleteOne();
        response.send(updatedUser);
    } catch (error) {
        next(error);
    }
}

