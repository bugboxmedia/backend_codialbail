
import {
    AddressNotFoundException,
} from './UserException';
import UpdateOrderProps from '../Shared/SharedProps/UpdateOrderProps';
import { orderModel, orderModelType } from '../../Models/Order/OrderModel';

export default async function UpdateOrderUseCase(id: string, props: UpdateOrderProps): Promise<orderModelType> {
    const order = await orderModel.findById(id);

    if (!order) throw new AddressNotFoundException();

    const updatedOrder = await orderModel.findByIdAndUpdate(id, props, { new: true });

    return updatedOrder;
}
