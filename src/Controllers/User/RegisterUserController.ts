import { Request, Response, NextFunction } from 'express';
import { userModel } from '../../Models/User/UserModel';
import { plainToClass } from 'class-transformer';
import { PasswordService } from '../../Utils/PasswordService';
import { UserWithThatEmailAlreadyExistsException } from '../Admin/AdminException';
import GenerateAccessAndRefreshToken, { IAccessAndRefreshToken } from '../Shared/GenerateAccessAndRefreshToken';
import RegisterUserProps from './props/RegisterUserProps';

export default async function RegisterUserController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(RegisterUserProps, request.body);

    try {
        const userExists = await userModel.findOne({ email: props.email });

        if (userExists) throw new UserWithThatEmailAlreadyExistsException(props.email);

        const hashedPassword = PasswordService.hash(props.password);

        const user = await userModel.create({
            ...props,
            password: hashedPassword
        });

        const tokens: IAccessAndRefreshToken = await GenerateAccessAndRefreshToken(user);

        const data = {
            data: user,
            token: tokens
        };

        response.send(data);
    } catch (error) {
        next(error);
    }
}
