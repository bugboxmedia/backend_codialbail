// Create Admin Controller Buisness Logic

import { plainToClass } from 'class-transformer';
import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { orderModel } from '../../Models/Order/OrderModel';
import UpdateOrderProps from '../Shared/SharedProps/UpdateOrderProps';
import { OrderNotFoundException, UserNotFoundException } from './UserException';

export default async function GetOrderByUserIdController(request: AuthRequest, response: Response, next: NextFunction) {
    
    try {

        const order = await orderModel.find({ userId: request.userId });

        if (!order) throw new OrderNotFoundException();

        response.send(order);
        
    } catch (error) {
        next(error);
    }
}
