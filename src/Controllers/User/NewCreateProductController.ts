import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import CreateProductPropsv2 from './props/NewCreateProductProps';
import { productsModel } from '../../Models/Products/ProductsModel';

export default async function CraeteProductControllerv2(request: Request, response: Response, next: NextFunction) {

    const props = plainToClass(CreateProductPropsv2, request.body);
    
    try {
        const user = await productsModel.create({
            ...props
        });

        response.send(user);
    } catch (error) {
        next(error);
    }
}
