import {  Response, NextFunction } from 'express';
import { productsModel } from '../../Models/Products/ProductsModel';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { ProductNotFoundException, UserNotFoundException } from './UserException';
import { userModel } from '../../Models/User/UserModel';

export default async function RemoveCartItemController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.pid;
    const user_id = request.userId;

    try {

        const product = await productsModel.findById(id);
        if (!product) throw new ProductNotFoundException();

        var productPrice = product.productPrice
        const user = await userModel.findById(user_id).select('+password');

        if (!user) throw new UserNotFoundException();
 
        let existingCart = user.cart
        let existingProduct ;
        let position ;
        for (let i = 0; i < existingCart['items'].length; i++) {
            if(existingCart['items'][i]['id'] == id){
                existingProduct = existingCart['items'][i];
                existingCart['itemCount'] = existingCart['itemCount'] - existingCart['items'][i]['quantity']
                existingCart['totalPrice'] = existingCart['totalPrice'] - ( existingCart['items'][i]['productPrice'] * existingCart['items'][i]['quantity'])
                position = i
            }
        }

        // existingCart["items"].forEach((item) => {
        //     if(item['id'] == id){
        //         existingProduct = item;
        //         existingCart['itemCount'] = existingCart['itemCount'] - item['quantity']
        //         existingCart['totalPrice'] = existingCart['totalPrice'] - ( item['productPrice'] * item['quantity'])
        //     }

        // })

        // let cartItem:Array<any> =  existingCart["items"]
        existingCart["items"].splice(position, 1);
                // existingCart["items"].remove(existingProduct)
        // existingCart['itemCount'] = existingCart['itemCount'] - 1;

        // existingCart['totalPrice'] -=  parseInt(productPrice)
        // User update with cart object

        const updatedUser = await userModel.findByIdAndUpdate(user_id, {
            cart : existingCart
        }, { new: true });
        // await product.deleteOne();
        response.send(updatedUser);
    } catch (error) {
        next(error);
    }
}

