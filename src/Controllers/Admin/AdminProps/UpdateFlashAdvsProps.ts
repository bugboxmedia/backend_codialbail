// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsOptional,
} from 'class-validator';

// Create and export AdminProps
class UpdateFlashAdvsProps {
    @IsOptional()
    isActive: boolean;
    
    @IsString()
    @IsOptional()
    advName: string;
   
    @IsString()
    @IsOptional()
    imgUrl: string;
    


}


export default UpdateFlashAdvsProps;
