// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsEnum,
    IsOptional,
    IsDefined
} from 'class-validator';
import { orderModelConditions } from '../../../Models/Order/OrderModelConditions';

// Create and export AdminProps
class UpdateOrderStatusProps {
    @IsOptional()
    userId: string;

    @IsDefined()
    @IsString()
    @IsEnum(orderModelConditions.orderStatus.permittedStrings, {
        message: 'Invalid Option'
    })
    orderStatus:  'Ordered' | 'Accepted' | 'On-Trasit' | 'Dispatched' | 'Cancelled' | 'Completed';
}


export default UpdateOrderStatusProps;
