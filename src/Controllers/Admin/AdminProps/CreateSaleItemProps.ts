// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsDefined,
    IsBoolean,
} from 'class-validator';


class CreateSaleItemProps {
    
    @IsDefined()
    @IsString()
    saleItemImg: string;
    
    @IsDefined()
    @IsString()
    saleItemDescription: string;

    @IsDefined()
    @IsString()
    saleItemName: string;

    @IsDefined()
    saleItemPrice: number;
    
    @IsDefined()
    @IsString()
    saleItemCategory: string;
    
    @IsDefined()
    @IsBoolean()
    isActive: boolean;
}


export default CreateSaleItemProps;
