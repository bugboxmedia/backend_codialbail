// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import { Type } from 'class-transformer';
import {
    IsString,
    Matches,
    IsDefined,
    IsNotEmptyObject,
    ValidateNested,
    IsEnum,
    IsPhoneNumber,
    MinLength,
    IsISO8601
} from 'class-validator';
import { userModelConditions } from '../../../Models/User/UserModelConditions';
import { validEmailRegex } from '../../../Utils/helper';

// Create and export AdminProps
class AddUserProps {
    @IsDefined()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => NameProps)
    name: NameProps;

    @IsDefined()
    @Matches(validEmailRegex)
    email: string;

    @IsDefined()
    @IsPhoneNumber('ZZZ')
    phoneNumber: string;
    // {
    //     message: '$property should be 10 digits'
    // }

    @IsDefined()
    @MinLength(userModelConditions.password.minLength)
    password: string;

    @IsDefined()
    @IsString()
    @IsEnum(userModelConditions.gender.permittedStrings, {
        message: '$property should be one of [$constraint1]'
    })
    gender: 'male' | 'female' | 'others';

    @IsDefined()
    @IsString()
    @IsEnum(userModelConditions.role.permittedStrings, {
        message: '$property should be one of [$constraint1]'
    })
    role: 'user' | 'admin' ;


    // @IsDefined()
    // @Matches(validPhoneOtpRegex)
    // otp: string;
}

class NameProps {
    @IsDefined()
    @IsString()
    firstName: string;

    @IsDefined()
    @IsString()
    lastName: string;
}

export default AddUserProps;
