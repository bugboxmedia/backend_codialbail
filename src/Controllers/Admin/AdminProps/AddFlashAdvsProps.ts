// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import { Type } from 'class-transformer';
import {
    IsString,
    IsDefined,
    IsBoolean
} from 'class-validator';

// Create and export AdminProps
class AddFlashAdvsProps {
    @IsDefined()
    @IsString()
    advName: string;
    
    @IsDefined()
    @IsString()
    imgUrl: string;

    @IsDefined()
    @IsBoolean()
    isActive: boolean;

}

export default AddFlashAdvsProps;
