// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsOptional    
} from 'class-validator';

class UpdateSaleItemProps {
    @IsOptional()
    @IsString()
    saleItemDescription: string;

    @IsOptional()
    @IsString()
    saleItemImg: string;

    @IsOptional()
    @IsString()
    saleItemName: string;

    @IsOptional()
    saleItemPrice: string;
    
    @IsOptional()
    @IsString()
    saleItemCategory: string;
    
    @IsOptional()
    isActive: boolean;

}


export default UpdateSaleItemProps;
