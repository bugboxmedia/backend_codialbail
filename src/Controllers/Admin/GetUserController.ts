import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { userModel } from '../../Models/User/UserModel';
import { UserNotFoundException } from '../User/UserException';

export default async function GetUserController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.userId;
    try {
        const user = await userModel.findById(id);

        if (!user) throw new UserNotFoundException();

        response.send(user);
    } catch (error) {
        next(error);
    }
}
