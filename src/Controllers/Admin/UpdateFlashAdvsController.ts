import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import UpdateFlashAdvsProps from './AdminProps/UpdateFlashAdvsProps';
import UpdateFlashAdvsUseCase from './UpdateFlashAdvsUseCase';
import { AdvNotFoundException } from '../User/UserException';

export default async function UpdateFlashAdvsController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.fid;
    const props = plainToClass(UpdateFlashAdvsProps, request.body);

    try {
        const flashAdv = await UpdateFlashAdvsUseCase(id, props);

        if (!flashAdv) throw new AdvNotFoundException();

        response.send('Updated Flash Adv');
    } catch (error) {
        next(error);
    }
}
