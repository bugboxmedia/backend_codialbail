import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { flashAdvsModel } from '../../Models/FlashAdvs/FlashAdvsModel';

export default async function GetAllAdvsController(request: AuthRequest, response: Response, next: NextFunction) {
    try {
        const flash = await flashAdvsModel.find();
        response.send({
            data: flash
        });
    } catch (error) {
        next(error);
    }
}