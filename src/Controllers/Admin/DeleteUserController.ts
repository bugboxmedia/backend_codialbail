import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { userModel } from '../../Models/User/UserModel';
import { UserNotFoundException } from '../User/UserException';
import { UserIsASuperAdminException } from './AdminException';

export default async function DeleteUserController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.userId;

    try {
        const user = await userModel.findById(id);

        if (!user) throw new UserNotFoundException();

        if (user.role === 'admin')
            throw new UserIsASuperAdminException('Hence cannot delete it.');

        await user.deleteOne();

        response.send(user);
    } catch (error) {
        next(error);
    }
}
