import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { productModel } from '../../Models/Product/ProductModel';
import CreateSaleItemProps from './AdminProps/CreateSaleItemProps';
import { saleItemModel } from '../../Models/SaleItems/SaleItemModel';

export default async function CraeteSaleItemController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(CreateSaleItemProps, request.body);

    try {
        const user = await saleItemModel.create({
            ...props
        });

        response.send(user);
    } catch (error) {
        next(error);
    }
}
