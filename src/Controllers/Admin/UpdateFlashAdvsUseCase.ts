
import {
    AdvNotFoundException,
    OrderNotFoundException,
} from '../User/UserException';
import UpdateFlashAdvsProps from './AdminProps/UpdateFlashAdvsProps';
import { flashAdvsModel, flashAdvsModelType } from '../../Models/FlashAdvs/FlashAdvsModel';

export default async function UpdateFlashAdvsUseCase(id: string, props: UpdateFlashAdvsProps): Promise<flashAdvsModelType> {
    const adv = await flashAdvsModel.findById(id);

    if (!adv) throw new AdvNotFoundException();

    const updatedAdv = await flashAdvsModel.findByIdAndUpdate(id, props, { new: true });

    return updatedAdv;
}
