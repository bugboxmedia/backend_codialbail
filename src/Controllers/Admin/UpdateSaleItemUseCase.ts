

import UpdateSaleItemProps from './AdminProps/UpdateSaleItemProps';
import { saleItemModel, saleItemModelType } from '../../Models/SaleItems/SaleItemModel';
import { SaleItemNotFoundException } from '../User/UserException';

export default async function UpdateSaleItemUseCase(id: string, props: UpdateSaleItemProps): Promise<saleItemModelType> {
    const SaleItem = await saleItemModel.findById(id);

    if (!SaleItem) throw new  SaleItemNotFoundException();

    const updatedSaleItem = await saleItemModel.findByIdAndUpdate(id, props, { new: true });

    return updatedSaleItem;
}
