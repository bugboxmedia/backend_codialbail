import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { SaleItemNotFoundException } from '../User/UserException';
import UpdateSaleItemProps from './AdminProps/UpdateSaleItemProps';
import UpdateSaleItemUseCase from './UpdateSaleItemUseCase';

export default async function UpdateSaleItemController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.pid;
    const props = plainToClass(UpdateSaleItemProps, request.body);

    try {
        const item = await UpdateSaleItemUseCase(id, props);

        if (!item) throw new SaleItemNotFoundException();

        response.send(item);
    } catch (error) {
        next(error);
    }
}
