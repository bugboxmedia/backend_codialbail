
import {
    OrderNotFoundException,
} from '../User/UserException';
import { orderModel, orderModelType } from '../../Models/Order/OrderModel';
import UpdateOrderStatusProps from './AdminProps/UpdateOrderStatusProps';

export default async function UpdateOrderStatusUseCase(id: string, props: UpdateOrderStatusProps): Promise<orderModelType> {
    const order = await orderModel.findById(id);

    if (!order) throw new OrderNotFoundException();

    const updatedOrder = await orderModel.findByIdAndUpdate(id, props, { new: true });

    return updatedOrder;
}
