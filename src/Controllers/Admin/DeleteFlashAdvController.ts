import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { flashAdvsModel } from '../../Models/FlashAdvs/FlashAdvsModel';
import { AdvNotFoundException } from '../User/UserException';

export default async function DeleteFlashAdvController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.fid;

    try {
        const adv = await flashAdvsModel.findById(id);

        if (!adv) throw new AdvNotFoundException();

        
        await adv.deleteOne();

        response.send(adv);
    } catch (error) {
        next(error);
    }
}
