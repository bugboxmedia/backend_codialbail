import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { UserNotFoundException } from '../User/UserException';
import UpdateOrderProps from '../Shared/SharedProps/UpdateOrderProps';
import UpdateOrderStatusUseCase from './UpdateOrderStatusUseCase';

export default async function UpdateOrderStatusController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.oid;
    const props = plainToClass(UpdateOrderProps, request.body);

    try {
        const user = await UpdateOrderStatusUseCase(id, props);

        if (!user) throw new UserNotFoundException();

        response.send(user);
    } catch (error) {
        next(error);
    }
}
