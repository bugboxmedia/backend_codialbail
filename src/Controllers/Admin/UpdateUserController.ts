import { Request, Response, NextFunction } from 'express';
import UpdateUserProps from './AdminProps/UpdateUserProps';
import { plainToClass } from 'class-transformer';
import UpdateUserUseCase from '../Shared/UpdateUserUseCase';
import { UserNotFoundException } from '../User/UserException';

export default async function UpdateUserController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.userId;
    const props = plainToClass(UpdateUserProps, request.body);

    try {
        const user = await UpdateUserUseCase(id, props);

        if (!user) throw new UserNotFoundException();

        response.send(user);
    } catch (error) {
        next(error);
    }
}
