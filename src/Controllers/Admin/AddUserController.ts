import { Request, Response, NextFunction } from 'express';
import AddUserProps from './AdminProps/AddUserProps';
import { userModel } from '../../Models/User/UserModel';
import { plainToClass } from 'class-transformer';
import { PasswordService } from '../../Utils/PasswordService';
import { UserWithThatEmailAlreadyExistsException } from './AdminException';

export default async function AddUserController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(AddUserProps, request.body);

    try {
        const userExists = await userModel.findOne({ email: props.email });

        if (userExists) throw new UserWithThatEmailAlreadyExistsException(props.email);

        const hashedPassword = PasswordService.hash(props.password);

        const user = await userModel.create({
            ...props,
            password: hashedPassword,
        });

        response.send(user);
    } catch (error) {
        next(error);
    }
}
