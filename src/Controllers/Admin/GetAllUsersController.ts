import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { userModel } from '../../Models/User/UserModel';

export default async function GetAllUsersController(request: AuthRequest, response: Response, next: NextFunction) {
    try {
        const users = await userModel.find();

        response.send(users);
    } catch (error) {
        next(error);
    }
}
