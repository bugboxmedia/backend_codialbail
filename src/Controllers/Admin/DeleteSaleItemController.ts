import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { saleItemModel } from '../../Models/SaleItems/SaleItemModel';
import {  SaleItemNotFoundException } from '../User/UserException';

export default async function DeleteSaleItemController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.sid;

    try {
        const saleItem = await saleItemModel.findById(id);

        if (!saleItem) throw new SaleItemNotFoundException();

        
        await saleItem.deleteOne();

        response.send(saleItem);
    } catch (error) {
        next(error);
    }
}
