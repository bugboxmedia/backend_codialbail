import { Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { orderModel } from '../../Models/Order/OrderModel';

export default async function GetAllUsersController(request: AuthRequest, response: Response, next: NextFunction) {
    try {
        const orders = await orderModel.find();

        response.send(orders);
    } catch (error) {
        next(error);
    }
}
