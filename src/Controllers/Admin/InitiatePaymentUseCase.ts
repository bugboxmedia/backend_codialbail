
import { parse } from 'path';
import razorpay from 'razorpay'
export default async function InitPaymentUseCase(id: string, totalPrice: string): Promise<string> {
    var instance = new razorpay({ key_id: process.env.RAZORPAY_KEY_ID, key_secret: process.env.RAZORPAY_KEY_SECRET })

    var options = {
        amount: parseInt(totalPrice)*100,  // amount in the smallest currency unit
        currency: "INR",
        receipt: id
    };
    var order_id;
    await instance.orders.create(options).then((order) => {
        console.log(order);
        order_id = order.id;
    }).catch((error) => {
        console.log(error);
    })

    return order_id;
}