import { Request, Response, NextFunction } from 'express';
import AddFlashAdvsProps from './AdminProps/AddFlashAdvsProps';
import { plainToClass } from 'class-transformer';
import { flashAdvsModel } from '../../Models/FlashAdvs/FlashAdvsModel';

export default async function AddFlashAdvsController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(AddFlashAdvsProps, request.body);

    try {

        const flash = await flashAdvsModel.create({
            ...props,
        });

        response.send(flash);
    } catch (error) {
        next(error);
    }
}
