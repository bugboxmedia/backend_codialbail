import { Request, Response, NextFunction } from 'express';
import { AdvNotFoundException } from '../User/UserException';
import { flashAdvsModel } from '../../Models/FlashAdvs/FlashAdvsModel';

export default async function GetAdvByIdController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.aid;

    try {
        const adv = await flashAdvsModel.findById(id);

        if (!adv) throw new AdvNotFoundException();

        response.send(adv);
    } catch (error) {
        next(error);
    }
}
