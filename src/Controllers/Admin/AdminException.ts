import HttpException from '../../Exceptions/HttpException';

export class UserWithThatEmailAlreadyExistsException extends HttpException {
    constructor(email: string) {
        super(409, `User with email ${email} already exists`);
    }
}

export class FabricWithThatNameAlreadyExistsException extends HttpException {
    constructor(name: string) {
        super(409, `Fabric with name ${name} already exists`);
    }
}

export class UserWithThatPhoneNumberAlreadyExistsException extends HttpException {
    constructor(phoneNumber: string) {
        super(409, `User with phoneNumber ${phoneNumber} already exists`);
    }
}

export class InvalidZoneIdException extends HttpException {
    constructor(zoneId: string) {
        super(404, `ZoneId ${zoneId} is Invalid`);
    }
}

export class InvalidThresholdKeyException extends HttpException {
    constructor(key: string) {
        super(400, `Key '${key}' is not present in threshold field.`);
    }
}

export class ZoneCannotBeAssignedToAdminException extends HttpException {
    constructor() {
        super(403, `Zone cannot be assigned to admin`);
    }
}

export class ZoneShouldBeAssignedToStaffException extends HttpException {
    constructor() {
        super(403, `Zone needs to be assigned to staff`);
    }
}

export class UserIsASuperAdminException extends HttpException {
    constructor(message: string) {
        super(403, `User is a Super Admin. ${message}`);
    }
}

export class FabricNotFoundException extends HttpException {
    constructor(fabricId: string) {
        super(404, `Fabric with id ${fabricId} Not Found`);
    }
}

export class LevelsExistsException extends HttpException {
    constructor(collectionName: string) {
        super(403, `${collectionName} cannot be deleted as it is referenced in Levels.`);
    }
}

export class DeviceExistsException extends HttpException {
    constructor(collectionName: string) {
        super(403, `${collectionName} cannot be deleted as it is referenced in Devices.`);
    }
}

export class UserExistsException extends HttpException {
    constructor(collectionName: string) {
        super(403, `${collectionName} cannot be deleted as it is referenced in Users.`);
    }
}

export class PatientTagExistsException extends HttpException {
    constructor(collectionName: string) {
        super(403, `${collectionName} cannot be deleted as it is referenced in PatientTag.`);
    }
}

export class MetricNotFoundException extends HttpException {
    constructor(metricId: string) {
        super(404, `Metric with id ${metricId} not found`);
    }
}
