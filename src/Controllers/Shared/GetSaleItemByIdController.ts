import { Request, Response, NextFunction } from 'express';
import { SaleItemNotFoundException } from '../User/UserException';
import { saleItemModel } from '../../Models/SaleItems/SaleItemModel';

export default async function GetSaleItemsByIdController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.pid;

    try {
        const saleItem = await saleItemModel.findById(id);

        if (!saleItem) throw new SaleItemNotFoundException();

        response.send(saleItem);
    } catch (error) {
        next(error);
    }
}
