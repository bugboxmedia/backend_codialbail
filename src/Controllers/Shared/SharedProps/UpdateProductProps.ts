// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsOptional    
} from 'class-validator';

class UpdateProductProps {
    @IsOptional()
    @IsString()
    productDescription: string;

    @IsOptional()
    @IsString()
    productImg: string;

    @IsOptional()
    @IsString()
    productBackImg: string;

    @IsOptional()
    @IsString()
    productName: string;

    @IsOptional()
    productPrice: string;
    
    @IsOptional()
    @IsString()
    productCategory: string;
    
    @IsOptional()
    isActive: boolean;
        
    @IsOptional()
    isWriter: boolean;

}


export default UpdateProductProps;
