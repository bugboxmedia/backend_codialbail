// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsOptional,
} from 'class-validator';

// Create and export AdminProps
class UpdateAddressProps {
    @IsOptional()
    userId: string;
    
    @IsOptional()
    @IsString()
    name: string;
    
    @IsOptional()
    @IsString()
    line1: string;
    
    @IsOptional()
    @IsString()
    line2: string;
    
    @IsOptional()
    @IsString()
    city: string;
    
    @IsOptional()
    @IsString()
    state: string;
    
    @IsOptional()
    @IsString()
    country: string;
    
    @IsOptional()
    @IsString()
    pincode: string;

    
}

export default UpdateAddressProps;
