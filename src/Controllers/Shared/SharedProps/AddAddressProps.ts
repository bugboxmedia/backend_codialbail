// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import { Type } from 'class-transformer';
import {
    IsString,
    IsDefined,
} from 'class-validator';

// Create and export AdminProps
class AddAddressProps {
    @IsDefined()
    userId: string;
    
    @IsDefined()
    @IsString()
    name: string;
    
    @IsDefined()
    @IsString()
    line1: string;
    
    @IsDefined()
    @IsString()
    line2: string;
    
    @IsDefined()
    @IsString()
    city: string;
    
    @IsDefined()
    @IsString()
    state: string;
    
    @IsDefined()
    @IsString()
    country: string;
    
    @IsDefined()
    @IsString()
    pincode: string;

    
}

export default AddAddressProps;
