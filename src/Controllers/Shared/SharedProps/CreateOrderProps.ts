// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import { Type } from 'class-transformer';
import {
    IsString,
    Matches,
    IsDefined,
    IsNotEmptyObject,
    ValidateNested,
    IsEnum,
    IsNumber,
    IsPhoneNumber,
    MinLength,
    IsISO8601
} from 'class-validator';
import { userModelConditions } from '../../../Models/User/UserModelConditions';
import { orderModelConditions } from '../../../Models/Order/OrderModelConditions';

// Create and export AdminProps
class CreateOrderProps {
    @IsDefined()
    userId: string;
    
    @IsString()
    addressID: string;
    
    @IsDefined()
    products: Array<String>;

    @IsDefined()
    @IsISO8601({ strict: false })
    orderedDate: Date;

    @IsDefined()
    @IsString()
    @IsEnum(orderModelConditions.paymentStatus.permittedStrings, {
        message: '$property should be one of [$constraint1]'
    })
    paymentStatus:  'Initiated' | 'Completed' | 'Failed';

    @IsDefined()
    @IsString()
    @IsEnum(orderModelConditions.orderStatus.permittedStrings, {
        message: '$property should be one of [$constraint1]'
    })
    orderStatus:  'Ordered' | 'Accepted' | 'On-Trasit' | 'Dispatched' | 'Cancelled' | 'Completed';
}

export default CreateOrderProps;
