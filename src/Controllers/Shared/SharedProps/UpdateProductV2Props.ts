// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsOptional,    
    IsBoolean
} from 'class-validator';

class UpdateProductV2Props {
    @IsOptional()
    @IsString()
    productDescription: string;

    @IsOptional()
    @IsString()
    productImg: string;

    @IsOptional()
    @IsString()
    productBackImg: string;

    @IsOptional()
    @IsString()
    productName: string;

    @IsOptional()
    productPrice: string;
    
    @IsOptional()
    @IsString()
    productCategory: string;

        
    @IsOptional()
    isWriter: boolean;

    @IsString()
    @IsOptional()
    productProps?: Map<string,string>;
    
    @IsBoolean()
    @IsOptional()
    isActive?: boolean;
        
}


export default UpdateProductV2Props;
