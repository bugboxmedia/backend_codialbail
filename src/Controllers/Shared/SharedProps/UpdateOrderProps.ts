// Type 'class transformer' requires this package
// Refer node.js installation => https://github.com/typestack/class-transformer#browser
import 'reflect-metadata';
import {
    IsString,
    IsEnum,
    IsOptional
} from 'class-validator';
import { orderModelConditions } from '../../../Models/Order/OrderModelConditions';

// Create and export AdminProps
class UpdateOrderProps {
    @IsOptional()
    userId: string;

    @IsOptional()
    @IsString()
    paymentRazorpayId: string

    @IsOptional()
    @IsString()
    paymentSignature: string

    @IsOptional()
    @IsString()
    @IsEnum(orderModelConditions.paymentStatus.permittedStrings, {
        message: 'Invalid Option'
    })
    paymentStatus:  'Initiated' | 'Completed' | 'Failed';

    @IsOptional()
    @IsString()
    @IsEnum(orderModelConditions.orderStatus.permittedStrings, {
        message: 'Invalid Option'
    })
    orderStatus:  'Ordered' | 'Accepted' | 'On-Trasit' | 'Dispatched' | 'Cancelled' | 'Completed';
}


export default UpdateOrderProps;
