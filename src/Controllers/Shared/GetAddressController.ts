
import { productModel, productModelType } from '../../Models/Product/ProductModel';
import { addressModel, addressModelType } from '../../Models/Address/AddressModel';
import { AddressNotFoundException } from '../User/UserException';
import { NextFunction, Request, Response } from 'express';

export default async function GetAddressController(request: Request, response: Response, next: NextFunction): Promise<void> {
    const id = request.params.oid;
    try {
        
        const Address = await addressModel.findById(id);
    
        if (!Address) throw new AddressNotFoundException();   
    
        response.send(Address);
    } catch (error) {
        next(error)
    }
}
