import { Request, Response, NextFunction } from 'express';
import { S3FileService } from '../../Utils/FileUpload/S3FileService';

export default async function UpdatePhotoController(request: Request, response: Response, next: NextFunction) {
    const fileService = new S3FileService('codialbail-press');
    try {
        const file = request.file;

        await fileService.uploadFile(file.path, `images/${file.filename}`, file.mimetype)

        // const address = await UpdateAddressUseCase(id, props);
        // if (!address) throw new UserNotFoundException();
        response.send({
            path: file.filename,
            message: "successfully uploaded"
        } )
        // response.send(address);
    } catch (error) {
        next(error);
    }
}
