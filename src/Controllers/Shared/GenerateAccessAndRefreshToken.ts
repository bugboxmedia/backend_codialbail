import { JWTToken, JWTClaims } from '../../Utils/AuthService/IJwt';
import { AuthService } from '../../Utils/AuthService/AuthService';
import { authConfig } from '../../config/Auth';
import { refreshTokenModel } from '../../Models/RefreshToken/RefreshTokenModel';
import { IUserProps } from '../../Models/User/IUserProps';

export interface IAccessAndRefreshToken {
    accessToken: JWTToken;
    refreshToken: JWTToken;
}

export default async function GenerateAccessAndRefreshToken(
    user: IUserProps | JWTClaims
): Promise<IAccessAndRefreshToken> {
    const accessToken = await AuthService.signAccessToken(user);
    const refreshToken = await AuthService.signRefreshToken(user);

    const expiryDate = new Date(); // creates a new Date
    expiryDate.setSeconds(authConfig.refreshTokenExpiryTime); // add expiryTime in seconds to it

    await refreshTokenModel.findOneAndUpdate(
        { userId: user._id },
        {
            token: refreshToken,
            expires: expiryDate
        },
        { upsert: true }
    );
    return { accessToken, refreshToken };
}
