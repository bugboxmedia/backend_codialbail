import { Request, Response, NextFunction } from 'express';
import { ProductNotFoundException } from '../User/UserException';
import { productsModel } from '../../Models/Products/ProductsModel';

export default async function GetSaleItemsController(request: Request, response: Response, next: NextFunction) {

    try {
        const products = await productsModel.find({
            isWriter: false
        });

        if (!products) throw new ProductNotFoundException();

        response.send(products);
    } catch (error) {
        next(error);
    }
}
