import { userModel, UserModelType } from '../../Models/User/UserModel';
import UpdateUserProps from '../Admin/AdminProps/UpdateUserProps';
import { PasswordService } from '../../Utils/PasswordService';
import {
    UserWithThatEmailAlreadyExistsException
} from '../Admin/AdminException';
import {
    UserNotFoundException,
    OldPasswordIsRequiredException,
    OldPasswordIsIncorrectException,
} from '../User/UserException';

export default async function UpdateUserUseCase(id: string, props: UpdateUserProps): Promise<UserModelType> {
    const user = await userModel.findById(id).select('+password');

    if (!user) throw new UserNotFoundException();

    if (props.email && !(user.email === props.email)) {
        const userExists = await userModel.findOne({ email: props.email });

        if (userExists) throw new UserWithThatEmailAlreadyExistsException(props.email);
    }

    // This is applicable only for retail users and not for dashboard staffs
    

    if (props.newPassword) {
        if (!props.oldPassword) throw new OldPasswordIsRequiredException();

        // check if the old password matches with the password saved in the db.
        const isPasswordMatched = await PasswordService.compare(props.oldPassword, user.password);

        if (!isPasswordMatched) throw new OldPasswordIsIncorrectException();
        props.password = PasswordService.hash(props.newPassword);
    }

    const updatedUser = await userModel.findByIdAndUpdate(id, props, { new: true });

    return updatedUser;
}
