

import UpdateProductProps from './SharedProps/UpdateProductProps';
import { productsModel, productsModelType } from '../../Models/Products/ProductsModel';
import { ProductNotFoundException } from '../User/UserException';
import UpdateProductV2Props from './SharedProps/UpdateProductV2Props';

export default async function UpdateProductUseV2Case(id: string, props: UpdateProductV2Props): Promise<productsModelType> {
    const Product = await productsModel.findById(id);

    if (!Product) throw new  ProductNotFoundException();

    const updatedSroduct = await productsModel.findByIdAndUpdate(id, props, { new: true });

    return updatedSroduct;
}
