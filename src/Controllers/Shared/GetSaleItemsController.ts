import { Request, Response, NextFunction } from 'express';
import { SaleItemNotFoundException } from '../User/UserException';
import { saleItemModel } from '../../Models/SaleItems/SaleItemModel';

export default async function GetSaleItemsController(request: Request, response: Response, next: NextFunction) {

    try {
        const saleItem = await saleItemModel.find();

        if (!saleItem) throw new SaleItemNotFoundException();

        response.send(saleItem);
    } catch (error) {
        next(error);
    }
}
