
import { productModel, productModelType } from '../../Models/Product/ProductModel';
import { addressModel, addressModelType } from '../../Models/Address/AddressModel';
import { AddressNotFoundException } from '../User/UserException';
import { NextFunction, Request, Response } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';

export default async function GetAllAddressesByUidController(request: AuthRequest, response: Response, next: NextFunction): Promise<void> {
    try {
        
        const Addresses = await addressModel.find({userId: request.userId});
    
        if (!Addresses) throw new AddressNotFoundException();   
    
        response.send(Addresses);
    } catch (error) {
        next(error)
    }
}
