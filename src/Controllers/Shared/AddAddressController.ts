import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import CreateOrderProps from './SharedProps/CreateOrderProps';
import { orderModel } from '../../Models/Order/OrderModel';
import { productModel } from '../../Models/Product/ProductModel';
import { ProductNotFoundException, UserNotFoundException } from '../User/UserException';
import AddAddressProps from './SharedProps/AddAddressProps';
import { addressModel } from '../../Models/Address/AddressModel';

export default async function AddAddressController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(AddAddressProps, request.body);

    try {
        const address = await addressModel.create({
            ...props,
        });
        response.send(address);
    } catch (error) {
        next(error);
    }
}
