import { Request, Response, NextFunction } from 'express';
import { ProductNotFoundException } from '../User/UserException';
import { productsModel } from '../../Models/Products/ProductsModel';

export default async function GetProductsByIdController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.pid;

    try {
        const products = await productsModel.find({
            _id: id
        });

        if (!products) throw new ProductNotFoundException();

        response.send(products);
    } catch (error) {
        next(error);
    }
}
