
import {
    OrderNotFoundException,
} from '../User/UserException';
import { orderModel, orderModelType } from '../../Models/Order/OrderModel';
import UpdateOrderProps from './SharedProps/UpdateOrderProps';

export default async function UpdateUserUseCase(id: string, props: UpdateOrderProps): Promise<orderModelType> {
    const order = await orderModel.findById(id);

    if (!order) throw new OrderNotFoundException();

    const updatedOrder = await orderModel.findByIdAndUpdate(id, props, { new: true });

    return updatedOrder;
}
