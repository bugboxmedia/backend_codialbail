import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { UserNotFoundException } from '../User/UserException';
import UpdateAddressProps from './SharedProps/UpdateAddressProps';
import UpdateAddressUseCase from './UpdateAddressUseCase';

export default async function UpdateAddressController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.oid;
    const props = plainToClass(UpdateAddressProps, request.body);

    try {
        const address = await UpdateAddressUseCase(id, props);

        if (!address) throw new UserNotFoundException();

        response.send(address);
    } catch (error) {
        next(error);
    }
}
