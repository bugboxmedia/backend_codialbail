

import UpdateProductProps from './SharedProps/UpdateProductProps';
import { productsModel, productsModelType } from '../../Models/Products/ProductsModel';
import { ProductNotFoundException } from '../User/UserException';

export default async function UpdateProductUseCase(id: string, props: UpdateProductProps): Promise<productsModelType> {
    const Product = await productsModel.findById(id);

    if (!Product) throw new  ProductNotFoundException();

    const updatedSroduct = await productsModel.findByIdAndUpdate(id, props, { new: true });

    return updatedSroduct;
}
