import { Request, Response, NextFunction } from 'express';
import { S3FileService } from '../../Utils/FileUpload/S3FileService';

export default async function UpdateAdvImageController(request: Request, response: Response, next: NextFunction) {
    const fileService = new S3FileService('codialbail-press');
    try {
        const file = request.file;

        await fileService.uploadFile(file.path, `images/flash/${file.filename}`, file.mimetype)

        response.send({
            path: file.filename,
            message: "successfully uploaded"
        } )
        // response.send(address);
    } catch (error) {
        next(error);
    }
}
