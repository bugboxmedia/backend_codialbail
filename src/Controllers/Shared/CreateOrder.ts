import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import CreateOrderProps from './SharedProps/CreateOrderProps';
import { orderModel } from '../../Models/Order/OrderModel';
import { ProductNotFoundException } from '../User/UserException';
import { productsModel } from '../../Models/Products/ProductsModel';
import InitPaymentUseCase from '../Admin/InitiatePaymentUseCase';

export default async function CreateOrderController(request: Request, response: Response, next: NextFunction) {
    const props = plainToClass(CreateOrderProps, request.body);

    try {
        var productList = [];
        var productCount;
        for (var [key, value] of Object.entries(props.products)){
            //get all products
            const productInfo = await productsModel.findById(value);

            if (!productInfo) throw new ProductNotFoundException();
            productList.push(productInfo)
        }
        var totalPrice;

        if(productList){
            productCount = productList.length;
            var tp = 0;
            for(var p in productList){
                tp = tp +  Number(productList[p]['productPrice'])
            }
            totalPrice = tp;
        }

        const order = await orderModel.create({ ...props, productCount, totalPrice });

        console.log('Order ID',order._id)
        var oId = (order._id).toString()
        const orderId = await InitPaymentUseCase(oId, order.totalPrice)
        const updatedOrder = await orderModel.findByIdAndUpdate(order._id, {paymentOrderId: orderId});
        order['paymentOrderId'] = orderId
        response.send(order);
    } catch (error) {
        next(error);
    }
}
