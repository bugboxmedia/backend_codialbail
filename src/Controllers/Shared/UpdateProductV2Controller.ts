import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { ProductNotFoundException } from '../User/UserException';
import UpdateProductUseV2Case from './UpdateProductV2UseCase';
import UpdateProductV2Props from './SharedProps/UpdateProductV2Props';

export default async function UpdateProductV2Controller(request: Request, response: Response, next: NextFunction) {
    const id = request.params.pid;
    const props = plainToClass(UpdateProductV2Props, request.body);

    try {
        const item = await UpdateProductUseV2Case(id, props);

        if (!item) throw new ProductNotFoundException();

        response.send(item);
    } catch (error) {
        next(error);
    }
}
