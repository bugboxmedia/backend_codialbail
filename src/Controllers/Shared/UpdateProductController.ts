import { Request, Response, NextFunction } from 'express';
import { plainToClass } from 'class-transformer';
import { ProductNotFoundException } from '../User/UserException';
import UpdateProductProps from '../Shared/SharedProps/UpdateProductProps';
import UpdateProductUseCase from './UpdateProductUseCase';

export default async function UpdateProductController(request: Request, response: Response, next: NextFunction) {
    const id = request.params.pid;
    const props = plainToClass(UpdateProductProps, request.body);

    try {
        const item = await UpdateProductUseCase(id, props);

        if (!item) throw new ProductNotFoundException();

        response.send(item);
    } catch (error) {
        next(error);
    }
}
