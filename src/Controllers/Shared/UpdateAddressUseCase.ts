
import {
    AddressNotFoundException,
} from '../User/UserException';
import UpdateAddressProps from './SharedProps/UpdateAddressProps';
import { addressModel, addressModelType } from '../../Models/Address/AddressModel';

export default async function UpdateAddressUseCase(id: string, props: UpdateAddressProps): Promise<addressModelType> {
    const address = await addressModel.findById(id);

    if (!address) throw new AddressNotFoundException();

    const updatedAddress = await addressModel.findByIdAndUpdate(id, props, { new: true });

    return updatedAddress;
}
