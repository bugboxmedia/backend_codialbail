import {  Response, NextFunction } from 'express';
import { AuthRequest } from '../../Middlewares/AuthMiddleware';
import { AddressNotFoundException } from '../User/UserException';
import { addressModel } from '../../Models/Address/AddressModel';

export default async function DeleteAddressController(request: AuthRequest, response: Response, next: NextFunction) {
    const id = request.params.oid;

    try {
        const address = await addressModel.findById(id);
        if (!address) throw new AddressNotFoundException();
        await address.deleteOne();
        response.send({
            data: "Address Deleted!"
        });
    } catch (error) {
        next(error);
    }
}

