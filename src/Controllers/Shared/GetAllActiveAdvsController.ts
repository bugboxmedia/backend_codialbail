import { Request, Response, NextFunction } from 'express';
import { flashAdvsModel } from '../../Models/FlashAdvs/FlashAdvsModel';

export default async function GetAllActiveAdvsController(request: Request, response: Response, next: NextFunction) {
    try {
        const flash = await flashAdvsModel.find();
        console.log('FlashAdvs',flash);

        let filteredFlashAdvs = flash.filter(val =>{
            return val["isActive"] === true  
        })
        
        response.send({
            data: filteredFlashAdvs
        });
    } catch (error) {
        next(error);
    }
}