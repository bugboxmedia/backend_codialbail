import { databaseUrl } from '../config/Database';
import mongoose from 'mongoose';
import { Db } from 'mongodb';

export default async function connectToTheDatabase(): Promise<Db> {
    // TODO: Remove this later.
    console.log(databaseUrl);
    try {
        const connection = await mongoose.connect(databaseUrl, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            // since this is not recommended for prodution scenerio (check official mongoose docs for more info)
            // because index builds can cause performance degradation.
            autoIndex: false,
            useFindAndModify: false
        });
        console.log('CONNECTED');

        return connection.connection.db;
    } catch (error) {
        console.error('Failed to connect to mongo on startup - retrying in 5 sec', error);
        // If there is error during inital connection to the db, we retry after 5 seconds.
        setTimeout(connectToTheDatabase, 5000);
    }
}
