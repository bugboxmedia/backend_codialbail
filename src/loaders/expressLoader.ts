import express from 'express';
import bodyparser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import { appConfigv1, appConfigv2 } from '../config/App';
import { router } from '../Routes';
import { routerv2 } from '../Routes/v2';
import errorMiddleware from '../Middlewares/ErrorMiddleware';

export default function initializeExpressMiddleware(app: express.Application): void {
    // Load Essential Middleware Packages
    // Overriding default 1mb limit to 50mb limit
    app.use(bodyparser.json({ limit: '50mb' }));
    app.use(bodyparser.urlencoded({ limit: '50mb', extended: true }));
    app.use(cors());
    app.use(helmet());

    // Load base Router
    app.use(appConfigv1.basePath, router);
    app.use(appConfigv2.basePath, routerv2);

    // Health Check Endpoint
    app.get('/ping', function (req, res) {
        // res.status(200).json({ message: 'pong', status:'200' });  });
        // res.statusCode = 200;
        res.json({ message: 'pong' });  });

    // Error Handler when no routes are matched
    app.use(function (req, res, next) {
        res.status(404).send({ message: 'Oops!! guess you wandered off to the wrong areas!!' });
        next();
    });

    // Error Middleware
    app.use(errorMiddleware);

    // Generic Error Handler
    app.use(function (err, req, res, next) {
        console.error(err.stack);
        res.status(500).json({
            message: err.message
        });
    });
}
