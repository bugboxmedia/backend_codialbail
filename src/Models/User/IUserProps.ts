export interface IUserProps {
    _id: string;
    name: INameProps;
    email: string;
    phoneNumber?: string;
    password: string;
    role?: 'user' | 'admin';
    gender?: 'male' | 'female' | 'others';
    isActive?: boolean;
    products?: Array<string>;
    cart: Object;
}

interface INameProps {
    firstName: string;
    lastName?: string;
}

// interface IPhoneProps {
//     countryCode: string;
//     phoneNumber: string;
// }
