const userModelConditions = {
    name: {
        minLength: 3
    },
    gender: {
        permittedStrings: ['male', 'female', 'others']
    },
    role: {
        permittedStrings: ['user', 'admin']
    },
    password: {
        minLength: 6
    }
};

export { userModelConditions };
