import mongoose, { Schema, Model, Document } from 'mongoose';
import { isValidEmail, isValidPhoneNumber } from '../../Utils/helper';
import { IUserProps } from './IUserProps';
import { userModelConditions } from './UserModelConditions';

const userSchema = new Schema(
    {
        name: {
            type: {
                firstName: {
                    type: String,
                    minlength: userModelConditions.name.minLength,
                    required: true
                },
                lastName: {
                    type: String,
                    required: true
                }
            },
            required: true
        },
        email: {
            type: String,
            lowercase: true,
            validate: {
                validator: function (email) {
                    return isValidEmail(email);
                },
                message: (props) => `${props.value} is not a valid email!`
            },
            unique: true,
            index: true
        },

        phoneNumber: {
            type: String,
            required: true
        },

        password: {
            type: String,
            select: false,
            minlength: userModelConditions.password.minLength,
            required: true
        },

        role: {
            type: String,
            enum: userModelConditions.role.permittedStrings,
            default: userModelConditions.role.permittedStrings[0] // user
        },
        
        gender: {
            type: String,
            required: true,
            enum: userModelConditions.gender.permittedStrings
        },

        isActive: {
            type: Boolean,
            default: true
        },

        products:{
            type: Array,
            default: [],
        },

        cart: { 
            type:{
                items: {
                    type: Array,
                    default: []
                },
                itemCount: {
                    type: Number,
                },
                totalPrice: {
                    type: String,
                }
            }
        }
    },
    {
        timestamps: true,
        toJSON: {
            versionKey: false,
            transform: function (doc, ret) {
                // delete password as we do not want to expose it to the user.
                delete ret.password;
                // we do not want to send createdAt and updatedAt to the user
                delete ret.createdAt;
                delete ret.updatedAt;
            }
        }
    }
);

// Create new model type
type UserModelType = IUserProps & Document;

type IUserModel = Model<UserModelType>;

// Create userModel
const userModel = mongoose.model<UserModelType>('user', userSchema);
// Export userModel and its type.
export { userModel, IUserModel, UserModelType };
