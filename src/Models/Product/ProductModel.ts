import mongoose, { Schema, Model, Document } from 'mongoose';
import { IProductProps } from './IProductProps';

const productSchema = new Schema(
    {
        userId: {
            ref: 'User',
            type: mongoose.Types.ObjectId,
            required: true
        },
        productProps: {
            type: Map
        },
        productImg: { 
            type: String, 
        },
        productBackImg: { 
            type: String, 
        },
        productCaption: {
            type: String
        },
        productPrice: {
            type: String
        },
    },
    {
        timestamps: true,
        toJSON: {
            versionKey: false,
            transform: function (doc, ret) {
                // we do not want to send createdAt and updatedAt to the user
                delete ret.createdAt;
                delete ret.updatedAt;
            }
        }
    }
);

// https://docs.mongodb.com/manual/core/2dsphere/
// Create a special 2dsphere index on `address.location`

// Create new model type
type productModelType = IProductProps & Document;

type IProductModel = Model<productModelType>;

// Create addressModel
const productModel = mongoose.model<productModelType>('products', productSchema);
// Export addressModel and its type.
export { productModel, IProductModel, productModelType };
