export interface IProductProps {
    _id: string;
    userId: string;
    productProps:Map<string, string>;
    productImg: string;
    productBackImg: string;
    productCaption: string;
    productPrice: string;
}