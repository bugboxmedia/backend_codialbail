import mongoose, { Schema, Model, Document } from 'mongoose';
import { IProductsProps } from './IProductsProps';

const productSchema = new Schema(
    {
        userId: {
            ref: 'User',
            type: mongoose.Types.ObjectId,
            required: true
        },
        productName: {
            type: String
        },
        productDescription: { 
            type: String, 
        },
        productCategory: {
            type: String
        },
        productPrice: {
            type: String
        },
        productImg: { 
            type: String, 
        },
        productBackImg: { 
            type: String, 
        },
        productProps: {
            type: Map
        },
        isActive: {
            type: Boolean
        },
        isWriter: {
            type: Boolean
        },
    },
    {
        timestamps: true,
        toJSON: {
            versionKey: false,
            transform: function (doc, ret) {
                // we do not want to send createdAt and updatedAt to the user
                delete ret.createdAt;
                delete ret.updatedAt;
            }
        }
    }
);

// https://docs.mongodb.com/manual/core/2dsphere/
// Create a special 2dsphere index on `address.location`

// Create new model type
type productsModelType = IProductsProps & Document;

type IProductsModel = Model<productsModelType>;

// Create addressModel
const productsModel = mongoose.model<productsModelType>('productsv2', productSchema);
// Export addressModel and its type.
export { productsModel, IProductsModel, productsModelType };
