export interface IProductsProps {
    _id: string;
    userId: string;
    productName: string;
    productDescription: string;
    productCategory: string;
    productPrice: string;
    productImg: string;
    productBackImg: string;
    productProps:Map<string, string>;
    isWriter: boolean;
    isActive: boolean;
}