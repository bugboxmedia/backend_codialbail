import mongoose, { Schema, Model, Document } from 'mongoose';
import { IOrderProps } from './IOrderProps';
import { orderModelConditions } from './OrderModelConditions';

const orderSchema = new Schema(
    {
        userId: {
            ref: 'User',
            type: mongoose.Types.ObjectId,
            required: true
        },
        addressID: {
            ref: 'Address',
            type: mongoose.Types.ObjectId,
            required: true
        },
        products: {
            type: Array,
            required: true
        },
        totalPrice: {
            type: String,
            required: true
        },
        productCount: {
            type: Number,
            required: true
        },
        orderedDate: {
            type: Date,
            required: true
        },
        paymentOrderId:{
            type: String,
            default: null
        },
        paymentRazorpayId:{
            type: String,
            default: null
        },
        paymentSignature:{
            type: String,
            default: null
        },
        paymentStatus:{
            type: String,
            enum: orderModelConditions.paymentStatus.permittedStrings,
            default: orderModelConditions.paymentStatus.permittedStrings[0]
        },
        orderStatus:{
            type: String,
            enum: orderModelConditions.orderStatus.permittedStrings,
            default: orderModelConditions.orderStatus.permittedStrings[0]
        },
    },
    {
        timestamps: true,
        toJSON: {
            versionKey: false,
            transform: function (doc, ret) {
                // we do not want to send createdAt and updatedAt to the user
                delete ret.createdAt;
                delete ret.updatedAt;
            }
        }
    }
);

// https://docs.mongodb.com/manual/core/2dsphere/
// Create a special 2dsphere index on `address.location`

// Create new model type
type orderModelType = IOrderProps & Document;

type IOrderModel = Model<orderModelType>;

// Create addressModel
const orderModel = mongoose.model<orderModelType>('orders', orderSchema);
// Export addressModel and its type.
export { orderModel, IOrderModel, orderModelType };
