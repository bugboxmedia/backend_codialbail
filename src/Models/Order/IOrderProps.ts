export interface IOrderProps {
    _id: string;
    userId: string;
    addressID: string;
    products: Array<any>;
    productCount: number;
    totalPrice: string;
    orderedDate: Date;
    paymentOrderId: string;
    paymentRazorpayId: string;
    paymentSignature:string;
    orderStatus: 'Ordered' | 'Accepted' | 'On-Trasit' | 'Dispatched' | 'Cancelled' | 'Completed';
    paymentStatus: 'Initiated' | 'Completed' | 'Failed';
}