const orderModelConditions = {
    name: {
        minLength: 3
    },
    orderStatus: {
        permittedStrings: ['Ordered' , 'Accepted' , 'On-Trasit' , 'Dispatched' , 'Cancelled' , 'Completed']
    },
    paymentStatus: {
        permittedStrings: ['Initiated' , 'Completed' , 'Failed']
    },
};

export { orderModelConditions };
