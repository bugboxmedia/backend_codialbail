import mongoose, { Schema, Model, Document } from 'mongoose';
import { IAddressProps } from './IAddressProps';

const addressSchema = new Schema(
    {
        userId: {
            ref: 'User',
            type: mongoose.Types.ObjectId,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        line1: {
            type: String,
            required: true
        },
        line2: {
            type: String,
            default: ''
        },
        city: {
            type: String,
            required: true
        },
        state: {
            type: String,
            required: true
        },
        country: {
            type: String,
            default: 'India'
        },
        pincode: {
            type: String,
            required: true
        },   
    },
    {
        timestamps: true,
        toJSON: {
            versionKey: false,
            transform: function (doc, ret) {
                // we do not want to send createdAt and updatedAt to the user
                delete ret.createdAt;
                delete ret.updatedAt;
            }
        }
    }
);

// https://docs.mongodb.com/manual/core/2dsphere/
// Create a special 2dsphere index on `address.location`

// Create new model type
type addressModelType = IAddressProps & Document;

type IAddressModel = Model<addressModelType>;

// Create addressModel
const addressModel = mongoose.model<addressModelType>('address', addressSchema);
// Export addressModel and its type.
export { addressModel, IAddressModel, addressModelType };
