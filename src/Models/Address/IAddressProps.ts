export interface IAddressProps {
    _id: string;
    userId: string;
    name: string;
    line1: string;
    line2?: string;
    city: string;
    state: string;
    country?: string;
    pincode: string;
}