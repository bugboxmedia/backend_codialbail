import mongoose, { Schema, Model, Document } from 'mongoose';
import { ISaleItemProps } from './ISaleItemProps';

const saleItemSchema = new Schema(
    {
        saleItemImg: { 
            type: String, 
        },
        saleItemDescription: { 
            type: String, 
        },
        saleItemName: {
            type: String
        },
        saleItemCategory: {
            type: String
        },
        saleItemPrice: {
            type: Number
        },
        isActive: {
            type: Boolean
        },
    },
    {
        timestamps: true,
        toJSON: {
            versionKey: false,
            transform: function (doc, ret) {
                // we do not want to send createdAt and updatedAt to the user
                delete ret.createdAt;
                delete ret.updatedAt;
            }
        }
    }
);

// https://docs.mongodb.com/manual/core/2dsphere/
// Create a special 2dsphere index on `address.location`

// Create new model type
type saleItemModelType = ISaleItemProps & Document;

type ISaleItemModel = Model<saleItemModelType>;

// Create addressModel
const saleItemModel = mongoose.model<saleItemModelType>('saleItems', saleItemSchema);
// Export addressModel and its type.
export { saleItemModel, ISaleItemModel, saleItemModelType };
