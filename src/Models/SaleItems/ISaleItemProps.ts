export interface ISaleItemProps {
    _id: string;
    saleItemName: string;
    saleItemDescription: string;
    saleItemCategory: string;
    saleItemImg: string;
    saleItemPrice: number;
    isActive: boolean;
}