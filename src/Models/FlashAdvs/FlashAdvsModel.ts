
import mongoose, { Schema, Model, Document } from 'mongoose';
import { IFlashAdvsProps } from './IFlashAdvsProps';

const FlashAdvsSchema = new Schema(
    {
        advName:{
            type: String,
            required: true
        },
        imgUrl:{
            type: String,
            required: true
        },
        isActive: {
            type: Boolean,
            required: true
        },
    },
    {
        timestamps: true,
        toJSON: {
            versionKey: false,
            transform: function (doc, ret) {
                // we do not want to send createdAt and updatedAt to the user
                delete ret.createdAt;
                delete ret.updatedAt;
            }
        }
    }
);

// https://docs.mongodb.com/manual/core/2dsphere/
// Create a special 2dsphere index on `address.location`

// Create new model type
type flashAdvsModelType = IFlashAdvsProps & Document;

type IFlashAdvsModel = Model<flashAdvsModelType>;

// Create addressModel
const flashAdvsModel = mongoose.model<flashAdvsModelType>('flashAdvss', FlashAdvsSchema);
// Export addressModel and its type.
export { flashAdvsModel, IFlashAdvsModel, flashAdvsModelType };
