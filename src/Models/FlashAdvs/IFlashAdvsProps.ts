export interface IFlashAdvsProps {
    _id: string;
    advName: string;
    imgUrl: string;
    isActive: boolean
}