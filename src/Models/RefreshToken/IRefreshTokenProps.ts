export interface IRefreshTokenProps {
    _id: string;
    userId: string;
    token: string;
    expires: Date;
}
