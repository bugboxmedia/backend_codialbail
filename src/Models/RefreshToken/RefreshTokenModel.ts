import mongoose, { Schema, Model, Document } from 'mongoose';
import { IRefreshTokenProps } from './IRefreshTokenProps';

const refreshTokenSchema = new Schema(
    {
        userId: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        token: {
            type: String,
            required: true
        },
        expires: {
            type: Date,
            required: true
        }
    },
    {
        toJSON: {
            virtuals: true,
            versionKey: false
        }
    }
);

refreshTokenSchema.virtual('isExpired').get(function () {
    return Date.now() >= this.expires;
});

// Create new model type
type RefreshTokenModelType = IRefreshTokenProps & Document;

type IRefreshTokenModel = Model<RefreshTokenModelType>;

// Create refreshTokenModel
const refreshTokenModel = mongoose.model<RefreshTokenModelType>('RefreshToken', refreshTokenSchema);
// Export refreshTokenModel and its type.
export { refreshTokenModel, IRefreshTokenModel, RefreshTokenModelType };