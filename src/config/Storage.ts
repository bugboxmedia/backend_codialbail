const storageConfig = {
    accessId: process.env.S3_ACCESS_ID,
    accessKey: process.env.S3_ACCESS_KEY,
    region: process.env.S3_REGION,
    fileBucket: process.env.S3_FILE_BUCKET_NAME,
    photoBucket: process.env.S3_PHOTO_BUCKET_NAME,
    email: process.env.SES_EMAIL
};

export { storageConfig };
