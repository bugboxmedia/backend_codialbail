const dbEnv = process.env.DB_ENV;

const databaseConfig = {
    host: process.env.DATABASE_HOST,
    databaseName: process.env.DATABASE_NAME,
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD
};

let dbType = 'mongodb+srv';
if (dbEnv === 'local') dbType = 'mongodb';

const databaseUrl = `${dbType}://${databaseConfig.username}:${databaseConfig.password}@${databaseConfig.host}`;

export { databaseConfig, databaseUrl };
