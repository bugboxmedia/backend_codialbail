const appConfigv1 = {
    version: 'v1',
    basePath: '/api/v1',
    language: 'en'
};

const appConfigv2 = {
    version: 'v2',
    basePath: '/api/v2',
    language: 'en'
};

export { appConfigv1, appConfigv2};
