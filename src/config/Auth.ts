const authConfig = {
    accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
    accessTokenExpiryTime: parseInt(process.env.ACCESS_TOKEN_EXPIRY_TIME) || 31536000, // 1 year
    refreshTokenSecret:
        process.env.REFRESH_TOKEN_SECRET ,
    refreshTokenExpiryTime: parseInt(process.env.REFRESH_TOKEN_EXPIRY_TIME) || 31536000, // 1 Year
    phoneOtpExpiryTime: parseInt(process.env.PHONE_OTP_EXPIRY_TIME), // 10 min
    smsApiKey: process.env.SMS_API_KEY,
    smsTemplate: process.env.SMS_TEMPLATE,
    passwordHashSalt: process.env.PASS_SALT
};

export { authConfig };
