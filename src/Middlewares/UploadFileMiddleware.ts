import multer, { StorageEngine } from 'multer';
import { RequestHandler } from 'express';

export class UploadFileMiddleware {
    private storage: StorageEngine;
    private filter;

    constructor() {
        this.storage = multer.diskStorage({
            destination: 'uploads/',
            filename: function (req, file, cb) {
                cb(null, new Date().valueOf() + '-' + file.originalname);
            }
        });

        this.filter = (req, file: Express.Multer.File, cb) => {
            // TODO filesize filter
            if (
                !(
                    file.mimetype === 'application/msword' ||
                    file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
                    file.mimetype === 'image/jpeg' ||
                    file.mimetype === 'image/jpg' ||
                    file.mimetype === 'image/png' ||
                    file.mimetype === 'application/pdf'
                )
            ) {
                return cb(new Error('Invalid File type'));
            }

            return cb(null, true);
        };
    }

    public uploadSingle(fieldName: string): RequestHandler {
        // console.log(fieldName);
        const upload = multer({ storage: this.storage, fileFilter: this.filter });
        return upload.single(fieldName);
    }
}
