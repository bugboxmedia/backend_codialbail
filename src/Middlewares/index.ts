import { UploadFileMiddleware } from './UploadFileMiddleware';
import { UploadPhotoMiddleware } from './UploadPhotoMiddleware';


const uploadFileMiddleWare = new UploadFileMiddleware();
const uploadPhotoMiddleWare = new UploadPhotoMiddleware();

export {  uploadFileMiddleWare, uploadPhotoMiddleWare };
