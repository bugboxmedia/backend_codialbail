import { Request, NextFunction, Response } from 'express';
import { JWTToken, JWTClaims } from '../Utils/AuthService/IJwt';
import AuthTokenMissingException from '../Exceptions/AuthTokenMissingException';
import InvalidAuthTokenException from '../Exceptions/InvalidAuthTokenException';
import { AuthService } from '../Utils/AuthService/AuthService';
import UserNotAnAdminException from '../Exceptions/UserNotAnAdminException';

export interface AuthRequest extends Request {
    userId: string;
    zoneId: string;
}

export async function isAuth(request: AuthRequest, response: Response, next: NextFunction) {
    try {
        const jwtClaims = await decodeAccessToken(request);
        request.userId = jwtClaims._id;
        next();
    } catch (error) {
        next(error);
    }
}

export async function isAdminAuth(request: AuthRequest, response: Response, next: NextFunction) {
    try {
        const jwtClaims = await decodeAccessToken(request);
        if (!(jwtClaims.role === 'admin')) throw new UserNotAnAdminException();

        const id = jwtClaims._id;
        request.userId = id;
        next();
    } catch (error) {
        next(error);
    }
}

async function decodeAccessToken(request: AuthRequest): Promise<JWTClaims> {
    const authHeader = request.headers['authorization'];

    if (!authHeader) throw new AuthTokenMissingException();

    const token: JWTToken = authHeader.split(' ')[1];
    const jwtClaims: JWTClaims = await AuthService.decodeAccessToken(token);

    if (!jwtClaims || !jwtClaims._id) throw new InvalidAuthTokenException();

    return jwtClaims;
}
