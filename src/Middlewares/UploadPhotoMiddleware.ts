import multer, { StorageEngine } from 'multer';
import { RequestHandler } from 'express';
import path from 'path';

export class UploadPhotoMiddleware {
    private storage: StorageEngine;
    private filter;

    constructor() {
        this.storage = multer.diskStorage({
            destination: 'uploads/',
            filename: function (req, file, cb) {
                let str = '';
                const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                const charactersLength = characters.length;
                for (let i = 0; i < 5; i++) {
                    str += characters.charAt(Math.floor(Math.random() * charactersLength));
                }

                cb(null, 'Photo-' + str + new Date().valueOf() + path.extname(file.originalname));
            }
        });

        this.filter = (req, file: Express.Multer.File, cb) => {
            // TODO filesize filter
            if (!(file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png')) {
                return cb(new Error('Invalid File type'));
            }

            return cb(null, true);
        };
    }

    public uploadSingle(fieldName: string): RequestHandler {
        // console.log(fieldName);
        const upload = multer({ storage: this.storage, fileFilter: this.filter });
        return upload.single(fieldName);
    }
}
