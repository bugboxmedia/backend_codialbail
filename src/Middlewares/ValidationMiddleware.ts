import { RequestHandler } from 'express';
import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import HttpException from '../Exceptions/HttpException';

export default function validateRequest<T>(
    type: any,
    forbidNonWhitelisted = true,
    skipMissingProperties = true
): RequestHandler {
    return (req, res, next) => {
        const props = { ...req.body };
        // https://github.com/typestack/class-validator/issues/305#issuecomment-504778830
        const whitelist = true;

        validate(plainToClass(type, props), { skipMissingProperties, whitelist, forbidNonWhitelisted }).then(
            (errors: ValidationError[]) => {
                if (errors.length > 0) {
                    const message = parseErrorMessageFromErrors(errors);
                    next(new HttpException(400, message));
                } else {
                    next();
                }
            }
        );
    };
}

function parseErrorMessageFromErrors(errors: ValidationError[]) {
    return errors
        .map((error: ValidationError) => {
            // this validation is needed while checking inside an object.
            // When checking inside an object error.constraints will not be present
            // child object will be present which will contain the the errors.
            if (error.children && error.children.length > 0) {
                return parseErrorMessageFromErrors(error.children);
            }
            return Object.values(error.constraints);
        })
        .join(', ');
}
