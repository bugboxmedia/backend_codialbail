import HttpException from './HttpException';

export class ParameterIsRequiredException extends HttpException {
    constructor(parameterName: string) {
        super(403, `${parameterName} is required.`);
    }
}

export class InternalServerError extends HttpException {
    constructor() {
        super(500, `Internal Server Error`);
    }
}

export class PhoneOtpNotFoundException extends HttpException {
    constructor(phoneNumber: string) {
        super(404, `Otp not found for the phoneNumber ${phoneNumber}`);
    }
}

export class PhoneOtpExpiredException extends HttpException {
    constructor() {
        super(401, `Otp has Expired`);
    }
}

export class IncorrectPhoneOtpException extends HttpException {
    constructor() {
        super(401, `Otp is Incorrect`);
    }
}
