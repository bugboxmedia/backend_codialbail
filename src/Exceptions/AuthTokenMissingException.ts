import HttpException from './HttpException';

class AuthTokenMissingException extends HttpException {
    constructor() {
        super(401, 'No authentication token provided');
    }
}

export default AuthTokenMissingException;
