import HttpException from './HttpException';

class UserNotAnAdminException extends HttpException {
    constructor() {
        super(401, 'User is not an Admin');
    }
}

export default UserNotAnAdminException;
