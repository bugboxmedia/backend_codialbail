import HttpException from './HttpException';

class InvalidAuthTokenException extends HttpException {
    constructor(msg?: string) {
        super(401, msg || 'Invalid Token');
    }
}

export default InvalidAuthTokenException;
