import express from "express";
import GetAllProductsController from "../../Controllers/Shared/GetAllProductsController";
import GetProductsByIdController from "../../Controllers/Shared/GetProductByIdController";
import { isAdminAuth, isAuth } from "../../Middlewares/AuthMiddleware";
import { UploadPhotoMiddleware } from "../../Middlewares/UploadPhotoMiddleware";
import { adminRouter } from "./AdminRoutes";
import { userRouter } from "./UserRoutes";

console.log('====================================');
console.log('v2');
console.log('====================================');

const routerv2 = express.Router();
// Retail User Specific End Points
// router.post('/user/register', validateRequest(RegisterUserProps), RegisterUserController);

// Dashboard Specific and Shared Endpoints
routerv2.get('/getAllProducts',  GetAllProductsController);
routerv2.get('/getProductById/:pid',  GetProductsByIdController);

routerv2.use('/admin', isAdminAuth, adminRouter);
routerv2.use('/', isAuth, userRouter);

export { routerv2 };
