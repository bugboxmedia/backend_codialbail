import express from 'express';
import UpdateProductV2Props from '../../Controllers/Shared/SharedProps/UpdateProductV2Props';
import UpdateProductV2Controller from '../../Controllers/Shared/UpdateProductV2Controller';
import NewCreateProductController from '../../Controllers/User/NewCreateProductController';
import UpdateProductProps from '../../Controllers/User/props/UpdateProductProps';
import UpdateProductController from '../../Controllers/User/UpdateProductController';
import validateRequest from '../../Middlewares/ValidationMiddleware';

const router = express.Router();

router.post('/product', NewCreateProductController);
router.patch('/product/:pid', validateRequest(UpdateProductV2Props), UpdateProductV2Controller);


export { router as userRouter };
