import express from 'express';
import DeleteProductV2Controller from '../../Controllers/User/DeleteProductV2Controller';


const router = express.Router();

// router.get('/', GetCurrentUserController);
// router.get('/users', GetAllUsersController);
// router.get('/user/:userId', GetUserController);
// router.get('/orders', GetOrdersController);
// router.get('/getAllAdvs', GetAllAdvsController);
// router.get('/getSaleItems', GetSaleItemsController);
// router.get('/getSaleItem/:pid', GetSaleItemsByIdController);
// router.get('/getAdv/:aid', GetAdvByIdController);
router.delete('/product/:pid', DeleteProductV2Controller);
// router.delete('/flashAdv/:fid', DeleteFlashAdvController);
// router.delete('/saleItem/:sid', DeleteSaleItemController);
// router.post('/user', validateRequest(AddUserProps), AddUserController);
// router.post('/flashAdv', validateRequest(AddFlashAdvsProps), AddFlashAdvsController);
// router.post('/saleItem', validateRequest(CreateSaleItemProps), CreateSaleItemController);
// router.patch('/flashAdv/:fid', validateRequest(UpdateFlashAdvsProps), UpdateFlashAdvsController);
// router.patch('/saleItem/:pid', validateRequest(UpdateSaleItemProps), UpdateSaleItemController);
// router.patch('/user/:userId', validateRequest(UpdateUserProps), UpdateUserController);
// router.patch('/order/status/:oid', validateRequest(UpdateOrderStatusProps), UpdateOrderStatusController);


export { router as adminRouter };
