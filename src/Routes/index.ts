import express from 'express';
import { adminRouter } from './AdminRoutes';
import validateRequest from '../Middlewares/ValidationMiddleware';
import UserLoginController from '../Controllers/User/UserLoginController';
import UserLogoutController from '../Controllers/User/UserLogoutConttroller';
import { isAdminAuth, isAuth } from '../Middlewares/AuthMiddleware';
import { userRouter } from './UserRoutes';
import RegisterUserController from '../Controllers/User/RegisterUserController';
import { RefreshTokenProp } from '../Controllers/User/props/RefreshTokenProp';
import RegisterUserProps from '../Controllers/User/props/RegisterUserProps';
import UserLoginProps from '../Controllers/User/props/UserLoginProps';
import GetAllActiveAdvsController from '../Controllers/Shared/GetAllActiveAdvsController';
import UploadPhotoController from '../Controllers/Shared/UploadPhotoController';
import { UploadPhotoMiddleware } from '../Middlewares/UploadPhotoMiddleware';
// import ResetPasswordController, { ResetPasswordProps } from '../Controllers/User/ResetPasswordController';

console.log('====================================');
console.log('v1');
console.log('====================================');

const router = express.Router();
const uploadPhoto = new UploadPhotoMiddleware;
// Retail User Specific End Points
router.post('/user/register', validateRequest(RegisterUserProps), RegisterUserController);

// Dashboard Specific and Shared Endpoints
router.post('/user/login', validateRequest(UserLoginProps), UserLoginController);
router.post('/user/logout', validateRequest(RefreshTokenProp), UserLogoutController);
router.get('/getAllActiveAdvs',  GetAllActiveAdvsController);
router.post('/upload', uploadPhoto.uploadSingle('file'), UploadPhotoController);
router.use('/admin', isAdminAuth, adminRouter);
router.use('/', isAuth, userRouter);

export { router };
