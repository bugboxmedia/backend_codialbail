import express from 'express';
import validateRequest from '../Middlewares/ValidationMiddleware';
import AddUserProps from '../Controllers/Admin/AdminProps/AddUserProps';
import AddUserController from '../Controllers/Admin/AddUserController';
import UpdateUserProps from '../Controllers/Admin/AdminProps/UpdateUserProps';
import UpdateUserController from '../Controllers/Admin/UpdateUserController';
import GetCurrentUserController from '../Controllers/User/GetCurrentUserController';
import GetAllUsersController from '../Controllers/Admin/GetAllUsersController';
import DeleteUserController from '../Controllers/Admin/DeleteUserController';
import GetUserController from '../Controllers/Admin/GetUserController';
import UpdateOrderStatusProps from '../Controllers/Admin/AdminProps/UpdateOrderStatusProps';
import UpdateOrderStatusController from '../Controllers/Admin/UpdateOrderStatusController';
import GetOrdersController from '../Controllers/Admin/GetOrdersController';
import AddFlashAdvsProps from '../Controllers/Admin/AdminProps/AddFlashAdvsProps';
import AddFlashAdvsController from '../Controllers/Admin/AddFlashAdvsController';
import UpdateFlashAdvsProps from '../Controllers/Admin/AdminProps/UpdateFlashAdvsProps';
import UpdateFlashAdvsController from '../Controllers/Admin/UpdateFlashAdvsController';
import GetAllAdvsController from '../Controllers/Admin/GetAllAdvsController';
import CreateSaleItemProps from '../Controllers/Admin/AdminProps/CreateSaleItemProps';
import UpdateSaleItemProps from '../Controllers/Admin/AdminProps/UpdateSaleItemProps';
import GetSaleItemsController from '../Controllers/Shared/GetSaleItemsController';
import GetSaleItemsByIdController from '../Controllers/Shared/GetSaleItemByIdController';
import CreateSaleItemController from '../Controllers/Admin/CreateSaleItemController';
import UpdateSaleItemController from '../Controllers/Admin/UpdateSaleItemController';
import GetAdvByIdController from '../Controllers/Admin/GetAdvByIdController';
import DeleteFlashAdvController from '../Controllers/Admin/DeleteFlashAdvController';
import DeleteSaleItemController from '../Controllers/Admin/DeleteSaleItemController';

const router = express.Router();

router.get('/', GetCurrentUserController);
router.get('/users', GetAllUsersController);
router.get('/user/:userId', GetUserController);
router.get('/orders', GetOrdersController);
router.get('/getAllAdvs', GetAllAdvsController);
router.get('/getSaleItems', GetSaleItemsController);
router.get('/getSaleItem/:pid', GetSaleItemsByIdController);
router.get('/getAdv/:aid', GetAdvByIdController);
router.delete('/user/:userId', DeleteUserController);
router.delete('/flashAdv/:fid', DeleteFlashAdvController);
router.delete('/saleItem/:sid', DeleteSaleItemController);
router.post('/user', validateRequest(AddUserProps), AddUserController);
router.post('/flashAdv', validateRequest(AddFlashAdvsProps), AddFlashAdvsController);
router.post('/saleItem', validateRequest(CreateSaleItemProps), CreateSaleItemController);
router.patch('/flashAdv/:fid', validateRequest(UpdateFlashAdvsProps), UpdateFlashAdvsController);
router.patch('/saleItem/:pid', validateRequest(UpdateSaleItemProps), UpdateSaleItemController);
router.patch('/user/:userId', validateRequest(UpdateUserProps), UpdateUserController);
router.patch('/order/status/:oid', validateRequest(UpdateOrderStatusProps), UpdateOrderStatusController);

export { router as adminRouter };
