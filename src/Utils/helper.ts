
const validEmailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
const validBloodGroupRegex = /^(A|B|AB|O)[+-]$/i;
const validPhoneNumberRegex = /^\d{10}$/;

const isValidEmail = (email: string) => {
    return email && validEmailRegex.test(email.toLowerCase());
};

const isValidPhoneNumber = (phoneNumber: string) => {
    return phoneNumber && validPhoneNumberRegex.test(phoneNumber);
};


const isValidBloodGroup = (bloodGroup: string) => {
    return bloodGroup && validBloodGroupRegex.test(bloodGroup);
};

// function areMapKeysEqual(map1: Map<string, string>, map2: Map<string, string>): boolean {
//     const keys1 = Object.keys(map1);
//     const keys2 = Object.keys(map2);

//     if (keys1.length !== keys2.length) {
//         return false;
//     }

//     for (const key of keys1) {
//         if (map1[key] !== map2[key]) {
//             return false;
//         }
//     }

//     return true;
// }

// Use this method carefully. This is created for a specific usecase.
function getInvalidKeyInMap(targetMap: any, inputMap: Map<string, string>): string {
    const keys1 = Object.keys(targetMap.toJSON());
    const keys2 = Object.keys(inputMap);

    for (const key of keys2) {
        if (!keys1.includes(key)) return key;
    }

    return null;
}

function getRequiredKeysNotInMap(requiredMap: any, inputMap: Map<string, string>): Array<string> {
    const keys1 = Object.keys(requiredMap.toJSON());
    const keys2 = Object.keys(inputMap);

    const requiredKeys = [];
    for (const key of keys1) {
        if (!keys2.includes(key)) requiredKeys.push(key);
    }

    return requiredKeys;
}

export {
    isValidEmail,
    validEmailRegex,
    validBloodGroupRegex,
    isValidBloodGroup,
    validPhoneNumberRegex,
    // areMapKeysEqual,
    getInvalidKeyInMap,
    getRequiredKeysNotInMap,
    isValidPhoneNumber
};
