import bcrypt from 'bcryptjs';
import { authConfig } from '../config/Auth';

export class PasswordService {
    public static hash(plainText: string): string {
        const salt = parseInt(authConfig.passwordHashSalt);
        return bcrypt.hashSync(plainText, salt);
    }

    public static async compare(plainTextPassword: string, hashedPassword: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            bcrypt.compare(plainTextPassword, hashedPassword, (err, compareResult) => {
                if (err) return resolve(false);
                return resolve(compareResult);
            });
        });
    }

    public static async verify(plainText: string, hashedPassword: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            bcrypt.compare(plainText, hashedPassword, (err, compareResult) => {
                if (err) return resolve(false);
                return resolve(compareResult);
            });
        });
    }
}
