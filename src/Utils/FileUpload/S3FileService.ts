import { FileService } from './FileService';
import aws, { S3 } from 'aws-sdk';
import fs from 'fs/promises';
import { storageConfig } from '../../config/Storage';

export class S3FileService implements FileService {
    private fileService: S3;
    private bucketName: string;
    constructor(bucketName: string) {
        aws.config.update({
            accessKeyId: storageConfig.accessId,
            secretAccessKey: storageConfig.accessKey,
            region: storageConfig.region
        });
        this.fileService = new aws.S3();
        this.bucketName = bucketName;
    }


    public async uploadFile(path: string, fileName: string, contentType?: string): Promise<void> {
        try {
            const fileData = await fs.readFile(path);
            const putParams = {
                Bucket: this.bucketName,
                Key: fileName,
                Body: fileData
            };

            if (contentType) {
                putParams['ContentType'] = contentType;
            }

            const uploadResult = await this.fileService.putObject(putParams).promise();

            await fs.unlink(path);

            console.log(uploadResult);

            return;
        } catch (err) {
            throw new Error(err);
        }
    }

    public async retriveFile(fileName: string): Promise<unknown> {
        try {
            const getParams = {
                Bucket: this.bucketName,
                Key: fileName
            };

            const fileData = await this.fileService.getObject(getParams).promise();

            console.log(fileData);

            return fileData.Body;
        } catch (err) {
            throw new Error(err);
        }
    }
}
