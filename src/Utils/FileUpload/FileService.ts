export interface FileService {
    uploadFile(path: string, fileName: string, contentType?: string): Promise<void>;
    retriveFile(fileNanme: string): Promise<unknown>;
}
