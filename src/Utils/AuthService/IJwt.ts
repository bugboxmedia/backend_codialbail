export interface JWTClaims {
    _id: string;
    role: string;
}

export type JWTToken = string;
