import jwt from 'jsonwebtoken';
import { JWTClaims, JWTToken } from './IJwt';
import { authConfig } from '../../config/Auth';
import { IUserProps } from '../../Models/User/IUserProps';
import { InternalServerError } from '../../Exceptions/GlobalException';
import InvalidAuthTokenException from '../../Exceptions/InvalidAuthTokenException';

export class AuthService {
    private static accessTokenSecret = authConfig.accessTokenSecret;
    private static refreshTokenSecret = authConfig.accessTokenSecret;

    public static signAccessToken(user: IUserProps | JWTClaims): Promise<JWTToken> {
        const claims: JWTClaims = {
            _id: user._id,
            role: user.role
        };

        const expiresIn = authConfig.accessTokenExpiryTime;

        return new Promise((resolve, reject) => {
            jwt.sign(claims, this.accessTokenSecret, { expiresIn }, (err, token) => {
                if (err) {
                    console.log(err.message);
                    reject(new InternalServerError());
                    return;
                }
                resolve(token);
            });
        });
    }

    public static async decodeAccessToken(token: JWTToken): Promise<JWTClaims> {
        return new Promise((resolve, reject) => {
            jwt.verify(token, this.accessTokenSecret, (err, decoded) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        reject(new InvalidAuthTokenException('Access Token Expired'));
                    }
                    const msg = err.name === 'JsonWebTokenError' ? 'Unauthorized' : err.message;
                    reject(new InvalidAuthTokenException(msg));
                    return;
                }
                return resolve(decoded as JWTClaims);
            });
        });
    }

    public static signRefreshToken(user: IUserProps | JWTClaims): Promise<JWTToken> {
        const claims: JWTClaims = {
            _id: user._id,
            role: user.role
        };

        const expiresIn = authConfig.refreshTokenExpiryTime;

        return new Promise((resolve, reject) => {
            jwt.sign(claims, this.refreshTokenSecret, { expiresIn }, (err, token) => {
                if (err) {
                    console.log(err.message);
                    reject(new InternalServerError());
                    return;
                }
                resolve(token);
            });
        });
    }

    public static async decodeRefreshToken(token: JWTToken): Promise<JWTClaims> {
        return new Promise((resolve, reject) => {
            jwt.verify(token, this.refreshTokenSecret, (err, decoded) => {
                if (err) {
                    console.log(err.message, err.name);
                    if (err.name === 'TokenExpiredError') {
                        reject(new InvalidAuthTokenException('Refresh Token Expired'));
                    }
                    reject(new InternalServerError());
                    return;
                }
                return resolve(decoded as JWTClaims);
            });
        });
    }
}
