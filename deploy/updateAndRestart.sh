#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
cd /home/ubuntu/codialbail_backend/

# clone the repo again
git pull git@gitlab.com:dhanushyrh/codialbail_backend.git

# TODO: Currently we are using staging branch. This needs to be changed later on to master
git checkout develop
git pull origin develop

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
source /home/ubuntu/.nvm/nvm.sh

# stop the previous pm2
pm2 kill
#npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
# npm install pm2 -g
# starting pm2 daemon
pm2 status


#install npm packages
echo "Running npm install"
npm install

#Restart the node server
pm2 start npm -- start
