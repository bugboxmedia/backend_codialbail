# This is multi stage build. In the first stage we install dependencies and 
# compile typescript code.
# In the second stage we copy the dependencies(node_modules) and compiled code
# from the first stage
FROM node:14.15.0-alpine3.12 AS builder

COPY . .

# https://github.com/kelektiv/node.bcrypt.js/issues/528#issuecomment-329975006
# RUN apk --no-cache add --virtual native-deps \
    # git g++ gcc libgcc libstdc++ linux-headers make python3 && \
    # npm install node-gyp -g &&\
#  RUN npm install npm@7
RUN npm install
    # npm rebuild bcrypt --build-from-source && \
    # npm cache clean --force &&\
    # apk del native-deps

RUN npm run build

FROM node:14.15.0-alpine3.12

WORKDIR /app

COPY --from=builder ./node_modules ./node_modules

COPY --from=builder ./dist ./dist

EXPOSE 8080

CMD ["node", "dist/app.js"]




